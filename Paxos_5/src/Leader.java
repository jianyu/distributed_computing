import java.util.*;

public class Leader extends Process {
	ProcessId[] acceptors;
	ProcessId[] replicas;
	BallotNumber ballot_number;
	boolean active = false;
	Map<Integer, Command> proposals = new HashMap<Integer, Command>();

        ProcessId[] leaders;
        boolean waitFlag; 
        // false: cannot ping with current leader
        // true: can ping with current leader
        
	public Leader(Env env, ProcessId me, ProcessId[] acceptors, ProcessId[] replicas, ProcessId[] leaders){
		this.env = env;
		this.me = me;
		ballot_number = new BallotNumber(0, me);
		this.acceptors = acceptors;
		this.replicas = replicas;
                
                this.leaders = leaders;
                this.waitFlag = false;
                
		env.addProc(me, this);
	}

        // for Problem #7
        public String toString() {
            return this.me + " : " + this.active + "    " + this.ballot_number;
        }
        
        public void setFlag(boolean flag) {
            waitFlag = flag;
        }
        
        public boolean getFlag() {
            return this.waitFlag;
        }
        
	public void body(){
		System.out.println("Here I am: " + me);

		new Scout(env, new ProcessId("scout:" + me + ":" + ballot_number),
			me, acceptors, ballot_number);
		for (;;) {
			PaxosMessage msg = getNextMessage();

			if (msg instanceof ProposeMessage) {
				ProposeMessage m = (ProposeMessage) msg;
				if (!proposals.containsKey(m.slot_number)) {
					proposals.put(m.slot_number, m.command);
                                        
                                        //System.out.println(m + " Receiving Process ID: " + this.me.toString());
                                        
					if (active) {
						new Commander(env,
							new ProcessId("commander:" + me + ":" + ballot_number + ":" + m.slot_number),
							me, acceptors, replicas, ballot_number, m.slot_number, m.command);
                                                //System.out.println("commander:" + me + ":" + ballot_number + ":" + m.slot_number + m);
                                                //System.out.println(m);
                                        }
				}
			} else if (msg instanceof AdoptedMessage) {
				AdoptedMessage m = (AdoptedMessage) msg;
                                
                                System.out.println(this.me + " receives ADOPTed Message!!!!");
                                
				if (ballot_number.equals(m.ballot_number)) {
					Map<Integer, BallotNumber> max = new HashMap<Integer, BallotNumber>();
					for (PValue pv : m.accepted) {
						BallotNumber bn = max.get(pv.slot_number);
						if (bn == null || bn.compareTo(pv.ballot_number) < 0) {
							max.put(pv.slot_number, pv.ballot_number);
							proposals.put(pv.slot_number, pv.command);
						}
					}

					for (int sn : proposals.keySet()) {
						new Commander(env,
							new ProcessId("commander:" + me + ":" + ballot_number + ":" + sn),
							me, acceptors, replicas, ballot_number, sn, proposals.get(sn));
                                                //System.out.println("commander:" + me + ":" + ballot_number + ":" + sn);
                                                //System.out.println(m);
					}
					active = true;
				}
			} else if (msg instanceof PreemptedMessage) {
                                
                                System.out.println(this.me + " receives a Preempted Message!");
				PreemptedMessage m = (PreemptedMessage) msg;
                                
				if (ballot_number.compareTo(m.ballot_number) < 0) {
                                    
                                    active = false;
                                    
                                    Leader_Detection leaderDetection = new Leader_Detection(env, acceptors, leaders);
                                    //System.out.println(this.me + " will call the getCurrentLeaderId fucntion!!!!!");
                                    ProcessId currentLeaderId = leaderDetection.getCurrentLeaderId();

                                    if (currentLeaderId.toString().equals("Fuck")) {
                                        waitFlag = false;
                                        System.out.println(this.me + " set waitFlag to be False!");
                                    } else {
                                        waitFlag = true;
                                        System.out.println(this.me + " set waitFlag to be True!");
                                        System.out.println(this.me + " will ping " + currentLeaderId);
                                    }

                                    Leader_comm leaderComm = new Leader_comm(env, new ProcessId(me.toString() + "_comm"),
                                            currentLeaderId, this.me);

                                    while (waitFlag) {
                                        //System.out.print(this + "dd       ");
                                        
                                        
                                        /*
                                         * Implementation #1
                                         * Just use a Null while loop
                                         */
                                        //System.out.print("");
                                        
                                        
                                        /* 
                                         * Implementation #2
                                         * Use currentLeaderScanning
                                         */
                                        currentLeaderId = leaderDetection.getCurrentLeaderId();
                                        leaderComm.setCurrentLeaderId(currentLeaderId);
                                        /*
                                         * You cannot do this currentLeaderId scanning
                                         * because this retrieves the information from the acceptors
                                         * where the state might be stale
                                         * because if an actual leader is died which is detected by this leader
                                         * it might be possible for the acceptors that they cannot detect it 
                                         * at this time (in time)
                                         * Hence, the correct way is to 
                                         * Oh My Godness, Forgive me, what I have said is totally wrong
                                         * Instead, you can use the currentLeaderId scanning
                                         */
                                    }

                                    System.out.println(this + "OUT!!!!!!OUT!!!!!!OUT!!!!!!OUT!!!!!!");

                                    ballot_number = new BallotNumber(m.ballot_number.round + 1, me);
                                    //System.out.println(ballot_number);
                                    new Scout(env, new ProcessId("scout:" + me + ":" + ballot_number),
                                            me, acceptors, ballot_number);
                                    
				}
                                
			} else if (msg instanceof PingMessage && !waitFlag) {
                            
                            PingMessage m = (PingMessage) msg; 
                            
                            ProcessId pingLeaderId = m.getSrc();
                            
                            System.out.println(this.me + " responses a PingMessage of " + pingLeaderId);
                            
                            try {
                                Thread.sleep(40);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            
                            sendMessage(pingLeaderId, new PingResponseMessage(me));
                            
                            //System.out.println(this.me + " receives a PING Message!!!!!");
                            
                        } else {
				System.err.println("Leader: unknown msg type");
			}
		}
	}
}
