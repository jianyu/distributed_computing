import java.util.*;

/**
 *
 * @author xwang
 */
public class Client {
    String clientName;
    Map<String, Account> accountList = new HashMap<String, Account>();
    
    public Client(String name) {
        this.clientName = name;
    }
    
    public void createAccount(String accountNum) {
        accountList.put(accountNum, new Account());
    }
    
    public void deposit(String accountNum, double deposit) {
        Account account = this.accountList.get(accountNum);
        account.deposit(deposit);
        this.accountList.put(accountNum, account);
    }
    
    public void withdraw(String accountNum, double withdraw) {
        Account account = this.accountList.get(accountNum);
        account.withdraw(withdraw);
        this.accountList.put(accountNum, account);
    }
    
    public double inquiry(String accountNum) {
        Account account = this.accountList.get(accountNum);
        return account.inquiry();
    }
    
    public String toString() {
        //System.out.println(this.clientName + ":");
        String output = "";
        for (String accountNum: this.accountList.keySet()) {
            String balanceToString = Double.toString(this.accountList.get(accountNum).getBalance());
            //System.out.println("Account Number: " + accountNumToString + " Balances: " + balanceToString);
            output += "Account Number: " + accountNum + " Balances: " + balanceToString + "\n";
        }
        return output;
    }
    
}
