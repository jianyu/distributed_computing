import java.util.*;

/**
 *
 * @author xwang
 */
public class Client_Listen extends Process {
    //Map<Integer, Account> accountList = new HashMap<Integer, Account>();
    ProcessId[] replicas;
    int req_id;
    boolean OpFlag;
    // OpFlag = false: there is no current operations, and a client can do something
    // OpFlag = true: there is a current transaction, and a client can do nothing
    
    public Client_Listen(Env env, ProcessId me, ProcessId[] replicas) {
        this.me = me;
        this.replicas = replicas;
        this.req_id = 0;
        this.env = env;
        this.OpFlag = false;
        
        env.addProc(me, this);
    }
    
     public void createAccount(int accountNum) {
        setOpFlag(); 
        // set to true, i.e., the client is current not available for new operations
        for (ProcessId r : replicas) {
            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, "create" + "@"
                                                                   + this.me.toString() + "@" + accountNum + "@"
                                                                   + req_id)));
        }
        req_id++;
    }

    public void deposit(int accountNum, double deposit) {
        setOpFlag();
        // set to true, i.e., the client is current not available for new operations
        for (ProcessId r: replicas) {
            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, 
                                                                   "deposit" + "@" 
                                                                   + this.me.toString() + "@" + accountNum + "@"
                                                                   + deposit + "@" + req_id)));
        }
        req_id ++;
    }
    
    public void withdraw(int accountNum, double withdraw) {
        setOpFlag();
        // set to true, i.e., the client is current not available for new operations
        for (ProcessId r: replicas) {
            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, 
                                                                   "withdraw" + "@" 
                                                                   + this.me.toString() + "@" + accountNum + "@"
                                                                   + withdraw + "@" + req_id)));
        }
        req_id ++;
    }
    
    public void transfer(int fromAccountNum, String toClientName, int toAccountNum, double transfer) {
        setOpFlag();
        for (ProcessId r: replicas) {
            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, 
                                                                   "transfer" + "@" 
                                                                   + this.me.toString() + "@" + fromAccountNum + "@"
                                                                   + toClientName + "@" + toAccountNum + "@" 
                                                                   + transfer + "@" + req_id)));
            //System.out.println(this.me.toString());
            //System.out.println(toClientName);
        }
        req_id ++;
    }
    
    public void sudoPrint() {
        setOpFlag();
        for (ProcessId r: replicas) {
            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, "sudoPrint")));
        }
        req_id ++;
    }

    public boolean getOpFlag () {
        return this.OpFlag;
    }
    
    private void setOpFlag () {
        this.OpFlag = true;
    }
    
    private void resetOpFlag () {
        this.OpFlag = false;
    }
    
    public void body() {
        System.out.println("Here I am: " + me);
        
        for (;;) {
            PaxosMessage msg = getNextMessage();
            
            if (msg instanceof ResponseMessage) {
                
                ResponseMessage m = (ResponseMessage) msg;
                Command getCommand = m.getCommand();
                
                String[] split = m.getCommand().getOp().toString().split("@");
                
                if (split[0].equals("create")) {
                    //if (split[3].equals(Integer.toString(req_id-1)) && this.getOpFlag()) {
                        this.resetOpFlag();
                    //}
                } else if (split[0].equals("deposit")) {
                    //if (split[4].equals(Integer.toString(req_id-1)) && this.getOpFlag()) {
                        this.resetOpFlag();
                    //}
                } else if (split[0].equals("withdraw")) {
                    //if (split[4].equals(Integer.toString(req_id-1)) && this.getOpFlag()) {
                        this.resetOpFlag();
                    //}
                } else if (split[0].equals("transfer")) {
                    //if (split[6].equals(Integer.toString(req_id-1)) && this.getOpFlag()) {
                        this.resetOpFlag();
                    //}
                } else if (split[0].equals("inquiry")) {
                    // maybe we need some shared variable to denote the balance
                    // we split the received reseponse cmd and get the balance
                    // the change the variable 
                    // so that the Client_Thread can retrieve this variable and feedback to the client
                    this.resetOpFlag();
                } else if (split[0].equals("sudoPrint")) {
                    this.resetOpFlag();
                }
            }
        }
    }
    
    
}
