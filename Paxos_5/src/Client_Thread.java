import java.util.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xwang
 */
public class Client_Thread extends Thread {
    Env env; 
    int proc_id;
    ProcessId[] replicas;
    
    
    // the proc_id is given by the user
    // env, proc_id and replicas will be passed to the lower level part of Client
    // i.e., Client_Listen
    // Client_Thread does not use these parameter, except for passing them to Client_Listen
    public Client_Thread(Env env, int proc_id, ProcessId[] replicas) {
        this.env = env;
        this.proc_id = proc_id;
        this.replicas = replicas;
    }
    
    public void run() {
        body();
    }
    
    public void body() {
        ProcessId clientpid = new ProcessId("Client:" + this.proc_id);
        // Assign ProcessId 
        Client_Listen client = new Client_Listen(this.env, clientpid, this.replicas);
        // Pass parameters to the lower level part of Client, i.e., Client_Listen
        
        // We can change the following steps by reading a corresponding file in terms of proc_id
        // instead of doing different things according to proc_id by if-conditionals
        
        /*
        if (this.proc_id == 0) {
            client.createAccount(0);
            if (client.exist(0) && !client.getOpFlag(0)) {
                client.deposit(0, 100);
            }
        } else if (this.proc_id ==1) {
            client.createAccount(1);
            if (client.exist(1) && !client.getOpFlag(1)) {
                client.deposit(1, 50);
            }
        }
         * 
         */
        
        // Operations
        // We can change the following steps by reading a corresponding file in terms of proc_id
        // instead of doing different things according to proc_id by if-conditionals
        if (this.proc_id == 0) {
            //while (!client.getOpFlag()) {}
            client.createAccount(0);
            //System.out.println("Hello");
            
            while (client.getOpFlag()) {
                System.out.print(""); 
                // we need something in this while loop
                // cannot implement null while loop
                // maybe because of java compiler's optimization
            }
            client.deposit(0, 200);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.deposit(0, 199);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.withdraw(0, 55);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.transfer(0, "Client:1", 0, 100);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.createAccount(1);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.deposit(1, 1000);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.sudoPrint();
            
        } else if (this.proc_id ==1) {
            client.createAccount(0);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.deposit(0, 50);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.deposit(0, 100);
            
            while (client.getOpFlag()) {
                System.out.print("");
            }
            client.withdraw(0, 50);
        }
        
    }
}
