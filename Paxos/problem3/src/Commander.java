
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Commander extends Process {

    ProcessId leader;
    ProcessId[] acceptors, replicas;
    BallotNumber ballot_number;
    int slot_number;
    Command command;

    public Commander(Env env, ProcessId me, ProcessId leader, ProcessId[] acceptors,
            ProcessId[] replicas, BallotNumber ballot_number, int slot_number, Command command) {
        this.env = env;
        this.me = me;
        this.acceptors = acceptors;
        this.replicas = replicas;
        this.leader = leader;
        this.ballot_number = ballot_number;
        this.slot_number = slot_number;
        this.command = command;
        env.addProc(me, this);
    }

    public void body() {
        P2aMessage m2 = new P2aMessage(me, ballot_number, slot_number, command);
        Set<ProcessId> waitfor = new HashSet<ProcessId>();
        for (ProcessId a : acceptors) {
            sendMessage(a, m2);
            waitfor.add(a);
        }

        while (2 * waitfor.size() >= acceptors.length) {
            PaxosMessage msg = getNextMessage();

            if (msg instanceof P2bMessage) {
                P2bMessage m = (P2bMessage) msg;

                if (ballot_number.equals(m.ballot_number)) {
                    if (waitfor.contains(m.src)) {
                        waitfor.remove(m.src);
                    }
                } else {
                    sendMessage(leader, new PreemptedMessage(me, m.ballot_number));
                    return;
                }
            }
        }

        for (ProcessId r : replicas) {
            DecisionMessage dcsMsg = new DecisionMessage(me, slot_number, command);
            env.logger.info(me + "send message to" + r + dcsMsg.toString());

            /*-----------------------------------------------
             * activate the code after perform()
            
            if (slot_number > 7) {
            try {
            Thread.sleep(5000);
            } catch (InterruptedException e) {
            e.printStackTrace();
            }
            }
            /*------------------------------------------------*/


            //System.out.println("********************" + "slot_num" + slot_number);


            if (env.normalflag == true) {
                sendMessage(r, dcsMsg);
            }
            if (env.holeflag == true) {
                if (slot_number <= 6) {
                    sendMessage(r, dcsMsg);
                } else if (slot_number == 7) {
                    if (r.toString().equals("replica:" + 0)) {
                        sendMessage(r, dcsMsg);
                    } else {
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        sendMessage(r, dcsMsg);
                    }
                } else {
                    sendMessage(r, dcsMsg);
                    //System.out.println(this.me + "uuuuuuuuu " + dcsMsg);
                }

            }
            
            if (env.middleholeflag == true) {
                if (slot_number == 7) {
                    if (r.toString().equals("replica:" + 0)) {
                        sendMessage(r, dcsMsg);
                    } else {
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        sendMessage(r, dcsMsg);
                    }
                } else {
                    sendMessage(r, dcsMsg);
                    //System.out.println(this.me + "uuuuuuuuuu " + dcsMsg);
                }

            }
            /*
            if (r.toString().equals("replica:" + 0)) {
            sendMessage(r, dcsMsg);
            } else {
            try {
            Thread.sleep(10000);
            } catch (InterruptedException e) {
            e.printStackTrace();
            }
            
            sendMessage(r, dcsMsg);
            }
             * 
             */
        }
    }
}
