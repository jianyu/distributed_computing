
import java.util.*;

public class Replica extends Process {

    ProcessId[] leaders;
    int slot_num = 1;
    Map<Integer /* slot number */, Command> proposals = new HashMap<Integer, Command>();
    Map<Integer /* slot number */, Command> decisions = new HashMap<Integer, Command>();
    
    Map<Integer /* slot number */, List<Command>> readonlys = new HashMap<Integer, List<Command>>();
    

    public Replica(Env env, ProcessId me, ProcessId[] leaders) {
        this.env = env;
        this.me = me;
        this.leaders = leaders;
        env.addProc(me, this);
    }

    void propose(Command c) {
        if (!decisions.containsValue(c)) {
            for (int s = 1;; s++) {
                if (!proposals.containsKey(s) && !decisions.containsKey(s)) {
                    proposals.put(s, c);
                    for (ProcessId ldr : leaders) {
                        sendMessage(ldr, new ProposeMessage(me, s, c));
                    }
                    break;
                }
            }
        }
    }

    void perform(Command c) {
        for (int s = 1; s < slot_num; s++) {
            if (c.equals(decisions.get(s))) {
                slot_num++;
                return;
            }
        }
        System.out.println("" + me + ": perform " + c);
        //System.out.println("" + slot_num);
        
        //env.logger.info("" + me + ": perform " + c);
        //env.logger.info("" + slot_num);
        
        //execute code here!!!!!!!!!!!!!!!!! <next, state> := op(state)
        slot_num++;

    }



    public void body() {
        System.out.println("Here I am: " + me);
        for (;;) {
            PaxosMessage msg = getNextMessage();

            if (msg instanceof RequestMessage) {
                
                RequestMessage m = (RequestMessage) msg;
                
                Operation opr = (Operation)m.command.op;
                if(opr instanceof Inquiry) {
                    if (!readonlys.containsKey(opr.slot_number)) {
                        List <Command> cmd = new ArrayList<Command>();
                        cmd.add(m.command);
                        this.readonlys.put(opr.slot_number, cmd);

                        //readonlys
                    } else {
                        List <Command> cmd = this.readonlys.get(opr.slot_number);
                        cmd.add(m.command);
                        this.readonlys.put(opr.slot_number, cmd);
                    }
                    
                    if (this.slot_num > opr.slot_number) {
                        
                        
                        
                    }
                    
                } else {
                    
                    propose(m.command);

                }
            } else if (msg instanceof DecisionMessage) {
                DecisionMessage m = (DecisionMessage) msg;
                
                env.logger.info(me+" receive "+m.toString());
                
                //System.out.println(me+" receive "+m.toString());
                
                decisions.put(m.slot_number, m.command);
                
                //System.out.println(m.slot_number +",,,,,,"+m.command);
                
                for (;;) {
                    Command c = decisions.get(slot_num);
                    if (c == null) {
                        break;
                    }
                    Command c2 = proposals.get(slot_num);
                    if (c2 != null && !c2.equals(c)) {
                        propose(c2);
                    }
                    perform(c);
                    
                    
                    
                    
                }
                
                
                
            } else {
                System.err.println("Replica: unknown msg type");
            }
        }
    }
}
