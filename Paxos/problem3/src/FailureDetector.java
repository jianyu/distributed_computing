/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jianyu
 */
public class FailureDetector extends Process {

    BallotNumber ballot_number = null;
    Set<PValue> accepted = new HashSet<PValue>();
    ProcessId currentLeaderId;
    ProcessId owner;

    public FailureDetector(Env env, ProcessId me, ProcessId currentLeaderId, ProcessId owner, BallotNumber ballot_number) {
        this.env = env;
        this.me = me;
        this.currentLeaderId = currentLeaderId;
        this.owner = owner;
        this.ballot_number = ballot_number;
        env.addProc(me, this);
    }

    public void body() {
        System.out.println("Here I am: " + me);

        for (;;) {
            
            System.out.println(me + " currentLeaderId: " + currentLeaderId);
            sendMessage(currentLeaderId, new PingMessage(me));

            //Thread.sleep(5000);
            
            /*
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(FailureDetector.class.getName()).log(Level.SEVERE, null, ex);
            }
            */

            int messageSize = this.getMessageSize(0);
            System.out.println(this.me.toString() + " MsgSize: " + messageSize);

            if (messageSize == 0) {
                System.out.println(me + " BallotNumber " + ballot_number.toString());
                sendMessage(owner, new BackFailureDetectorMessage(this.me, ballot_number));
                break;
                //exit(0);
                //return;
            } else {
                PaxosMessage msg = getNextMessage();
                continue;
            }

        }
    }
}
