/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author jianyu
 */
public class Bank extends Replica {

    //Map<Integer, Account> accountList = new HashMap<Integer, Account>();
    Map<String, Client> clientList = new HashMap<String, Client>();

    public Bank(Env env, ProcessId me, ProcessId[] leaders) {
        super(env, me, leaders);
    }

    @Override
    void perform(Command c) {

        //System.out.println(c);

        for (int s = 1; s < slot_num; s++) {

            //System.out.println(s +":-----" + decisions.get(s));

            if (c.equals(decisions.get(s))) {
                slot_num++;
                return;
            }
        }
        System.out.println("" + me + ": perform " + c);
        //System.out.println("" + slot_num);

        //env.logger.info("" + me + ": perform " + c);
        //env.logger.info("" + slot_num);

        //execute code here!!!!!!!!!!!!!!!!! <next, state> := op(state)
        slot_num++;


        Operation op = (Operation) c.op;
        double result = -2;

        if (op instanceof Create) {
            Create createOp = (Create) op;
            if (!clientList.containsKey(createOp.clientID)) {
                Client clientTmp = new Client(createOp.clientID);
                int accountNumTmp = createOp.accountID;
                clientTmp.createAccount(accountNumTmp);
                this.clientList.put(createOp.clientID, clientTmp);
            } else {
                Client clientTmp = clientList.get(createOp.clientID);
                int accountNumTmp = createOp.accountID;
                clientTmp.createAccount(accountNumTmp);
                this.clientList.put(createOp.clientID, clientTmp);
            }
            result = 0;
        } else if (op instanceof Deposit) {
            Deposit depositOp = (Deposit) op;

            if (clientList.containsKey(depositOp.clientID)) {
                if ((clientList.get(depositOp.clientID)).getAccountList().containsKey(depositOp.accountID)) {
                    System.out.println("ClientID" + depositOp.clientID + "AccountID" + depositOp.accountID);
                    result = clientList.get(depositOp.clientID).deposit(depositOp.accountID, depositOp.amount);
                } else {
                    //env.logger.
                    System.out.println("No such account!");
                }
            } else {
                System.out.println("No such client!");
            }
        } else if (op instanceof Withdraw) {
            Withdraw withdrawOp = (Withdraw) op;
            if (clientList.containsKey(withdrawOp.clientID)) {
                if (clientList.get(withdrawOp.clientID).getAccountList().containsKey(withdrawOp.accountID)) {
                    System.out.println("ClientID" + withdrawOp.clientID + "AccountID" + withdrawOp.accountID);
                    result = clientList.get(withdrawOp.clientID).withdraw(withdrawOp.accountID, withdrawOp.amount);
                } else {
                    System.out.println("No such account!");
                }
            } else {
                System.out.println("No such client!");
            }

        } else if (op instanceof Transfer) {
            Transfer transferOp = (Transfer) op;
            if (!clientList.containsKey(transferOp.fromClientID) || !clientList.containsKey(transferOp.toClientID)) {
                //env.logger.
                System.out.println("No such account!");
            } else {
                if (!clientList.get(transferOp.fromClientID).getAccountList().containsKey(transferOp.fromAccountID) || clientList.get(transferOp.toClientID).getAccountList().containsKey(transferOp.toAccountID)) {
                } else {
                    result = clientList.get(transferOp.fromClientID).withdraw(transferOp.fromAccountID, transferOp.amount);
                    result = clientList.get(transferOp.toClientID).deposit(transferOp.toAccountID, transferOp.amount);
                }
            }
        } else if (op instanceof Inquiry) {//inquiry


            Inquiry inquiryOp = (Inquiry) op;
            if (clientList.containsKey(inquiryOp.clientID)) {
                if ((clientList.get(inquiryOp.clientID)).getAccountList().containsKey(inquiryOp.accountID)) {
                    result = clientList.get(inquiryOp.clientID).inquiry(inquiryOp.accountID);
                } else {
                    //env.logger.
                    System.out.println("No such account!");
                }
            } else {
                System.out.println("No such client!");
            }
        }

        System.out.println(this.me + "haha_result:" + result);
        ResponseMessage m = new ResponseMessage(this.me, result, c, this.slot_num - 1);
        sendMessage(c.client, m);

    }

    public String DecisionToString() {
        String output = "Decision set: " + "\n";

        for (int slot_Num : this.decisions.keySet()) {
            output += this.decisions.get(slot_Num).toString() + "\n";
        }

        return output;

    }

    public String toString() {
        String output = "";
        for (String client : this.clientList.keySet()) {
            Client clientToString = this.clientList.get(client);
            output += client + "\n" + clientToString;
        }

        return output;
    }

    public void body() {
        System.out.println("Here I am: " + me);
        for (;;) {
            PaxosMessage msg = getNextMessage();

            if (msg instanceof RequestMessage) {

                RequestMessage m = (RequestMessage) msg;

                Operation opr = (Operation) m.command.op;
                if (opr instanceof Inquiry) {
                    if (!readonlys.containsKey(opr.slot_number)) {
                        List<Command> cmd = new ArrayList<Command>();
                        cmd.add(m.command);
                        this.readonlys.put(opr.slot_number, cmd);

                        //readonlys
                    } else {
                        List<Command> cmd = this.readonlys.get(opr.slot_number);
                        cmd.add(m.command);
                        this.readonlys.put(opr.slot_number, cmd);
                    }
                    
                    List<Integer> removeList = new ArrayList<Integer>();
                    
                    for (int readonly_slotnum : readonlys.keySet()) {
                        if (this.slot_num > readonly_slotnum) {
                            List<Command> cmdList = readonlys.get(readonly_slotnum);
                            for (Command cmd : cmdList) {

                                double result = 0;
                                Inquiry inquiryOp = (Inquiry) cmd.op;
                                if (clientList.containsKey(inquiryOp.clientID)) {
                                    if ((clientList.get(inquiryOp.clientID)).getAccountList().containsKey(inquiryOp.accountID)) {
                                        result = clientList.get(inquiryOp.clientID).inquiry(inquiryOp.accountID);
                                    } else {
                                        //env.logger.
                                        System.out.println("No such account!");
                                    }
                                } else {
                                    System.out.println("No such client!");
                                }


                                System.out.println(this.me + "haha_result:" + result);
                                ///////////////////////////////////////////////////////////////////////????????????????????????
                                ResponseMessage mm = new ResponseMessage(this.me, result, cmd, inquiryOp.slot_number);
                                sendMessage(cmd.client, mm);
                            }

                            //readonlys.remove(readonly_slotnum);
                            removeList.add(readonly_slotnum);

                        }
                    }
                    
 
                    for (int i : removeList) {
                        List<Command> cmdList2 = readonlys.get(i);
                        cmdList2.clear();
                        readonlys.remove(i);
                    }

                        /*
                    if (this.slot_num > opr.slot_number) {

                        double result = 0;

                        Inquiry inquiryOp = (Inquiry) opr;
                        if (clientList.containsKey(inquiryOp.clientID)) {
                            if ((clientList.get(inquiryOp.clientID)).getAccountList().containsKey(inquiryOp.accountID)) {
                                result = clientList.get(inquiryOp.clientID).inquiry(inquiryOp.accountID);
                            } else {
                                //env.logger.
                                System.out.println("No such account!");
                            }
                        } else {
                            System.out.println("No such client!");
                        }
                        
                        System.out.println(this.me + "haha_result:" + result);
                        ///////////////////////////////////////////////////////////////////////????????????????????????
                        ResponseMessage mm = new ResponseMessage(this.me, result, m.command, opr.slot_number);
                        sendMessage(m.command.client, mm);
                        
                        
                        List <Command> cmdList = this.readonlys.get(opr.slot_number);
                        
                        
                        
                    }
                    //remove
                    */
                } else {

                    propose(m.command);

                }
            } else if (msg instanceof DecisionMessage) {
                DecisionMessage m = (DecisionMessage) msg;

                env.logger.info(me + " receive " + m.toString());

                //System.out.println(me+" receive "+m.toString());

                decisions.put(m.slot_number, m.command);

                //System.out.println(m.slot_number +",,,,,,"+m.command);

                for (;;) {
                    Command c = decisions.get(slot_num);
                    if (c == null) {
                        break;
                    }
                    Command c2 = proposals.get(slot_num);
                    if (c2 != null && !c2.equals(c)) {
                        propose(c2);
                    }
                    perform(c);
                    
                    List<Integer> removeList = new ArrayList<Integer>();
                    
                    
                    for (int readonly_slotnum : readonlys.keySet()) {
                        
                        //for (int a : readonlys.keySet()) {
                        //    //System.out.println(a + "555555");
                        //    for (Command c1 : readonlys.get(a)) {
                         //       System.out.println(c1);
                         //   }
                       // }
                        
                        
                        //System.out.println(slot_num + "ccccccccc");
                        if (this.slot_num > readonly_slotnum) {
                            List<Command> cmdList = readonlys.get(readonly_slotnum);
                            //System.out.println(slot_num + "cccccccccdddd");
                            for (Command cmd : cmdList) {
                                
                                double result = -2;
                                Inquiry inquiryOp = (Inquiry) cmd.op;
                                if (clientList.containsKey(inquiryOp.clientID)) {
                                    if ((clientList.get(inquiryOp.clientID)).getAccountList().containsKey(inquiryOp.accountID)) {
                                        result = clientList.get(inquiryOp.clientID).inquiry(inquiryOp.accountID);
                                    } else {
                                        //env.logger.
                                        System.out.println("No such account!");
                                    }
                                } else {
                                    System.out.println("No such client!");
                                }

                                System.out.println(this.me + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@haha_result:" + result);
                                ///////////////////////////////////////////////////////////////////////????????????????????????
                                
                                ResponseMessage mm = new ResponseMessage(this.me, result, cmd, inquiryOp.slot_number);
                                
                                System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@" + mm);
                                //System.out.println(cmd.client);
                                //System.out.println(mm);
                                sendMessage(cmd.client, mm);
                                //System.out.println("Client" + cmd.client + "slot_num" + slot_num + "cccccccccddddeeeee");
                            }
                            //System.out.println("*****************************");
                            
                            
                            removeList.add(readonly_slotnum);
                            /*
                            List<Command> cmdList2 = readonlys.get(readonly_slotnum);
                            cmdList2.clear();
                            readonlys.remove(readonly_slotnum);
                            System.out.println("*****************************");
                            
                            for (int a : readonlys.keySet()) {
                            System.out.println(a + "555555");
                            for (Command c1 : readonlys.get(a)) {
                                System.out.println(c1);
                            }
                             */
                             
                            
                            
                        }
                            
                            
                        }
                    
                        for (int i: removeList) {
                            List<Command> cmdList2 = readonlys.get(i);
                            cmdList2.clear();
                            readonlys.remove(i);
                    
                    }


                }



            } else {
                System.err.println("Replica: unknown msg type");
            }
        }
    }
}
