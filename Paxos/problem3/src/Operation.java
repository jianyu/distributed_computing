/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class Operation {
    int slot_number;
    
}

class Create extends  Operation {
    String clientID;
    int accountID;

    public Create (String clientID, int accountID) {
        this.clientID = clientID;
        this.accountID = accountID;
    }
    public String toString() {
        return "Create(clientID:" + clientID + "accountID:" + accountID + ")";
    }
    
}

class Deposit extends Operation {
    String clientID;
    int accountID;
    double amount;
    
    public Deposit (String clientID, int accountID, double amount) {
        this.clientID = clientID;
        this.accountID = accountID;
        this.amount = amount;
    }
    public String toString() {
        return "Deposit(clientID:" + clientID + "clientID" + accountID +"amount:"+amount+")";
    }
}

class Withdraw extends Operation {
    String clientID;
    int accountID;
    double amount;
    public Withdraw (String clientID, int accountID, double amount) {
        this.clientID = clientID;
        this.accountID = accountID;
        this.amount = amount;
    }
    public String toString() {
        return "Withdraw(clientID" + clientID + "accountID:" + accountID +"amount:"+amount+")";
    }
}

class Transfer extends Operation {
    String fromClientID;
    int fromAccountID;
    String toClientID;
    int toAccountID;
    double amount;
    
    public Transfer (String fromClientID, int fromAccountID, String toClientID, int toAccountID, double amount) {
        this.fromClientID = fromClientID;
        this.fromAccountID = fromAccountID;
        this.toClientID = toClientID;
        this.toAccountID = toAccountID;
        this.amount = amount;
    }
    
    public String toString() {
        return "Transfer(fromClientID" + fromClientID +"fromAccountID:" + fromAccountID +"toClientID" +toClientID +"toAccountID:" + toAccountID +"amount:"+amount+")";
    }

}

class Inquiry extends Operation {
    String clientID;
    int accountID;
    
    public Inquiry (String clientID, int accountID) {
        this.clientID = clientID;
        this.accountID = accountID;
    }
    public String toString() {
        return "Inquiry(clientID" + clientID + "accountID:" + accountID + ")";
    }
    
}


