
import java.util.*;

public class Leader extends Process {

    ProcessId[] acceptors;
    ProcessId[] replicas;
    ProcessId[] leaders;
    BallotNumber ballot_number;
    boolean active = false;
    Map<Integer, Command> proposals = new HashMap<Integer, Command>();

    public Leader(Env env, ProcessId me, ProcessId[] acceptors, ProcessId[] replicas, ProcessId[] leaders) {
        this.env = env;
        this.me = me;
        this.ballot_number = new BallotNumber(0, me);
        this.acceptors = acceptors;
        this.replicas = replicas;
        this.leaders = leaders;
        env.addProc(me, this);
    }

    public String toString() {
        return "ballot_number:" + this.ballot_number.toString() + " active: " + this.active;
    }

    public void printAllLeadersActive() {
        System.out.println("****************************************************");
        for (ProcessId leaderId : env.leaders) {
            Leader leaderTmp = (Leader) env.procs.get(leaderId);
            System.out.println(leaderId + "-------" + leaderTmp.toString());
        }
        System.out.println("****************************************************");
    }

    public void body() {
        System.out.println("Here I am: " + me);

        env.logger.info(me + " born a Scout in the beginning:" + "scout:" + me + ":" + ballot_number);
        new Scout(env, new ProcessId("scout:" + me + ":" + ballot_number), me, acceptors, ballot_number);


        int failureDetectorCount = 0;
        //BallotNumber known_ballot_number;
        for (;;) {
            PaxosMessage msg = getNextMessage();

            //env.logger.info(me+" receive "+msg.toString());

            if (msg instanceof ProposeMessage) {
                ProposeMessage m = (ProposeMessage) msg;

                if (!proposals.containsKey(m.slot_number)) {
                    proposals.put(m.slot_number, m.command);
                    if (active) {

                        for (Integer sn : proposals.keySet()) {
                            env.logger.info(me + "after receive ProposeMsg&&active, proposals::slot_number:" + sn + " command: " + proposals.get(sn));
                        }

                        env.logger.info(me + " is active and born a Commander after receive ProposeMessage" + "commander:" + me + ":" + ballot_number + ":" + m.slot_number + " command " + m.command);
                        new Commander(env,
                                new ProcessId("commander:" + me + ":" + ballot_number + ":" + m.slot_number),
                                me, acceptors, replicas, ballot_number, m.slot_number, m.command);
                    }
                }
            } else if (msg instanceof AdoptedMessage) {
                AdoptedMessage m = (AdoptedMessage) msg;

                env.logger.info(me + " receive " + m.toString());

                if (ballot_number.equals(m.ballot_number)) {

                    for (Integer sn : proposals.keySet()) {
                        env.logger.info(me + "beforemax::proposals::slot_number:" + sn + " command: " + proposals.get(sn));
                    }

                    Map<Integer, BallotNumber> max = new HashMap<Integer, BallotNumber>();
                    for (PValue pv : m.accepted) {
                        //the code here is too beautiful!!!!!!!      bn --> the max ballot_number for specific slot_num in max<slot_num, ballot_number> set
                        BallotNumber bn = max.get(pv.slot_number);
                        if (bn == null || bn.compareTo(pv.ballot_number) < 0) {
                            max.put(pv.slot_number, pv.ballot_number);          //update max<slot_num, ballot_number> set. Reduce the overhead to come into if...Also necessary. Only bigger than max, can update.
                            proposals.put(pv.slot_number, pv.command);
                        }
                    }

                    for (Integer sn : max.keySet()) {
                        env.logger.info(me + "max::slot_number:" + sn + " ballot_number: " + max.get(sn));
                    }

                    for (Integer sn : proposals.keySet()) {
                        env.logger.info(me + "aftermax::proposals::slot_number:" + sn + " command: " + proposals.get(sn));
                    }

                    for (int sn : proposals.keySet()) {
                        env.logger.info(me + " born a Commander after receive AdoptedMessage" + "commander:" + me + ":" + ballot_number + ":" + sn + " command " + proposals.get(sn));
                        new Commander(env,
                                new ProcessId("commander:" + me + ":" + ballot_number + ":" + sn),
                                me, acceptors, replicas, ballot_number, sn, proposals.get(sn));
                    }
                    env.logger.info(me + " turn to " + "active:true");
                    //System.out.println(me + " turn to " + "active:true");
                    boolean activeTmp = active;
                    active = true;
                    if (activeTmp == false) {
                        printAllLeadersActive();
                    }
                    
                    // System.out.println("1111111111111111111111111");
                    
                }
            } else if (msg instanceof PreemptedMessage) {
                PreemptedMessage m = (PreemptedMessage) msg;

                env.logger.info(me + " receive " + m.toString());

                if (ballot_number.compareTo(m.ballot_number) < 0) {
                    boolean activeTmp = active;
                    
                    active = false;
                    //System.out.println("2222222222222222222222");
                    if (activeTmp == true) {
                        printAllLeadersActive();
                    }
                    //env.logger.info(me + "turn to " + "active:false");

                    //System.out.println(me + "turn to " + "active:false");

                    //need to be changed...
                    //ProcessId currentLeaderId = m.src;

/*
                    String[] split = m.src.name.split(":");
                    //System.out.println("..............................."+split[1]+":"+split[2]);
                    ProcessId currentLeaderId = new ProcessId(split[1] + ":" + split[2]);
                     * 
                     */
                    //ProcessId currentLeaderId = 
                    //currentLeaderId = leaderDetection.getCurrentLeaderId();
                    
              

                    

                    //System.out.println("")

                    if (failureDetectorCount == 0) {
                        FailureDetector failureDetector = new FailureDetector(env, new ProcessId("failureDetector:" + me + ":" + m.ballot_number + ":" + failureDetectorCount), this.me, m.ballot_number);
                    }
                    //sentFailureDetectorFlag = true;
                    failureDetectorCount++;



                    /*
                    //ballot_number++ -> to let the leader active and get the control authority
                    ballot_number = new BallotNumber(m.ballot_number.round + 1, me);
                    env.logger.info(me + " born a Scout after receive PreemptedMessage"+"scout:" + me + ":" + ballot_number);
                    new Scout(env, new ProcessId("scout:" + me + ":" + ballot_number), me, acceptors, ballot_number);
                    env.logger.info(me + "turn to "+"active:false");
                     * 
                     */

                }
            } else if (msg instanceof BackFailureDetectorMessage) {

                //include the known_ballot_number in this message?, thus......
                BackFailureDetectorMessage m = (BackFailureDetectorMessage) msg;
                failureDetectorCount = 0;

                //System.out.println(me + " FailureDetectorMessage----->BallotNumber:" + m.ballot_number);
                //ballot_number++ -> to let the leader active and get the control authority
                ballot_number = new BallotNumber(m.ballot_number.round + 1, me);
                
                System.out.println(me + "'s Failure Detector is Dying----->    BallotNumber:" + m.ballot_number);
                env.logger.info(me + " born a Scout after receive PreemptedMessage" + "scout:" + me + ":" + ballot_number);
                new Scout(env, new ProcessId("scout:" + me + ":" + ballot_number), me, acceptors, ballot_number);


            } else if (msg instanceof PingMessage) {

                PingMessage m = (PingMessage) msg;

                /*
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                 * */
                
                if (active) {
                    sendMessage(m.src, new PingResponseMessage(this.me));
                }
            } else {
                System.err.println("Leader: unknown msg type");
            }
        }
    }
}
