/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;



public class Client {
    String clientName;
    Map<Integer, Account> accountList = new HashMap<Integer, Account>();
    
    public Client(String name) {
        this.clientName = name;
    }
    
    public void createAccount(int accountNum) {
        accountList.put(accountNum, new Account());
    }
    
    public Map<Integer, Account> getAccountList() {
        return this.accountList;
    }
    
    public double deposit(int accountNum, double deposit) {
        double result = 0;
        Account account = this.accountList.get(accountNum);
        result = account.deposit(deposit);
        this.accountList.put(accountNum, account);
        return result;
    }
    
    public double withdraw(int accountNum, double withdraw) {
        double result = 0;
        Account account = this.accountList.get(accountNum);
        result = account.withdraw(withdraw);
        this.accountList.put(accountNum, account);
        return result;
    }
    
    public double inquiry(int accountNum) {
        double result = 0;
        Account account = this.accountList.get(accountNum);
        result = account.inquiry();
        return result;
    }
    
    public String toString() {
        //System.out.println(this.clientName + ":");
        String output = "";
        for (int accountNum: this.accountList.keySet()) {
            String balanceToString = Double.toString(this.accountList.get(accountNum).inquiry());
            //System.out.println("Account Number: " + accountNumToString + " Balances: " + balanceToString);
            output += "Account Number: " + accountNum + " Balances: " + balanceToString + "\n";
        }
        return output;
    }
    
}
