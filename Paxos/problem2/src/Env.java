
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Env {

    public Logger logger = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
    
    public final static int nAcceptors = 5, nReplicas = 3, nLeaders = 5, nRequests = 5, nClients = 2;
    ProcessId[] acceptors = new ProcessId[nAcceptors];
    ProcessId[] replicas = new ProcessId[nReplicas];
    ProcessId[] leaders = new ProcessId[nLeaders];
    ProcessId[] clients = new ProcessId[nClients];
    Map<ProcessId, Process> procs = new HashMap<ProcessId, Process>();
    //boolean subnetflag = false;
     boolean subnetflag = true;
    //boolean normalflag = true;
     boolean normalflag = false;
    
    Set<ProcessId> subnet1 = new HashSet<ProcessId>();
    Set<ProcessId> subnet2 = new HashSet<ProcessId>();
    

    synchronized void sendMessage(ProcessId dst, PaxosMessage msg) {
        Process p = procs.get(dst);
        if (p != null) {
            p.deliver(msg);
        }
    }
    
    synchronized void sendMessage2(ProcessId dst, PaxosMessage msg) {
        Client_Thread p = (Client_Thread)procs.get(dst);
        if (p != null) {
            p.deliver2(msg);
        }
    }

    synchronized void addProc(ProcessId pid, Process proc) {
        procs.put(pid, proc);
        proc.start();
    }

    synchronized void removeProc(ProcessId pid) {
        procs.remove(pid);
    }

    void run(String[] args) {
        logger.setLevel(Level.WARNING);
        logger.warning("test");

        for (int i = 0; i < nAcceptors; i++) {
            acceptors[i] = new ProcessId("acceptor:" + i);
            Acceptor acc = new Acceptor(this, acceptors[i]);
        }
        /*
        for (int i = 0; i < nReplicas; i++) {
            replicas[i] = new ProcessId("replica:" + i);
            Replica repl = new Replica(this, replicas[i], leaders);
        }*/
        
        for (int i = 0; i < nReplicas; i++) {
            replicas[i] = new ProcessId("replica:" + i);
            Bank repl = new Bank(this, replicas[i], leaders);
        }
        /*
        for (int i = 0; i < nLeaders; i++) {
            leaders[i] = new ProcessId("leader:" + i);
            Leader leader = new Leader(this, leaders[i], acceptors, replicas, leaders);
        }*/
        for (int i = 0; i < nLeaders; i++) {
            leaders[i] = new ProcessId("leader:" + i);
        }
        for (int i = 0; i < nLeaders; i++) {
            Leader leader = new Leader(this, leaders[i], acceptors, replicas, leaders);
        }
        
        for (int i = 0; i < nLeaders; i++) {
            if (i < nLeaders / 2) {
                subnet1.add(leaders[i]);
            } else {
                subnet2.add(leaders[i]);
            }
        }

        
        
        for (int i = 0; i < nClients; i++) {
            clients[i] = new ProcessId("client:" + i);
            Client_Thread client = new Client_Thread(this, clients[i], replicas);
        }

        ProcessId pid0 = new ProcessId("client_input:" + 0);
        //ProcessId pid1 = new ProcessId("client_input:" + 1);
        Operation op = new Create(clients[0].toString(),0);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 0, op)));
        op = new Deposit(clients[0].toString(), 0, 200);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 1, op)));
        op = new Deposit(clients[0].toString(), 0, 199);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 2, op)));
        op = new Withdraw(clients[0].toString(), 0, 129);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 3, op)));
        
        op = new Deposit(clients[0].toString(), 0, 200);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 4, op)));
        op = new Deposit(clients[0].toString(), 0, 199);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 5, op)));
        op = new Withdraw(clients[0].toString(), 0, 129);
        sendMessage2(clients[0], new ExecutionreqMessage(pid0, new Command(pid0, 6, op)));
        
        op = new Create(clients[1].toString(), 1);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 0, op)));
        op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 1, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 2, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 3, op)));
        
        op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 4, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 5, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 6, op)));
        
        op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));
        
                op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));
        
                op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));
        
                op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));
        
                op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));
        
                op = new Deposit(clients[1].toString(), 1, 20);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 7, op)));
        op = new Deposit(clients[1].toString(), 1, 19);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 8, op)));
        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));

        try {
            Thread.sleep(8000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Env.class.getName()).log(Level.SEVERE, null, ex);
        }

        op = new Withdraw(clients[1].toString(), 1, 12);
        sendMessage2(clients[1], new ExecutionreqMessage(pid0, new Command(pid0, 9, op)));


        /*
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Env.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        System.out.println("--------------------------------------------------------------------------------------");
        for (ProcessId replicaId : replicas) {
            Bank replicaTmp = (Bank) procs.get(replicaId);
            System.out.println(replicaId + replicaTmp.DecisionToString());
            System.out.println(replicaId + replicaTmp.toString());
        }
        
        System.out.println("--------------------------------------------------------------------------------------");
        for (ProcessId leaderId : leaders) {
            Leader leaderTmp = (Leader) procs.get(leaderId);
            System.out.println(leaderId +"-------" + leaderTmp.toString());
        }
         * 
         * */


        /*
        for (int i = 1; i < nRequests; i++) {
        ProcessId pid = new ProcessId("client:" + i);
            for (int r = 0; r < nReplicas; r++) {
                
                RequestMessage reqMessage = new RequestMessage(pid, new Command(pid, 0, "operation " + i));
                //logger.info(pid + " send message " + reqMessage + " to " + replicas[r]);
                sendMessage(replicas[r], reqMessage);
            }
        }
         */
        
        
        
        /*
        int cid; int r;
        ProcessId pid0 = new ProcessId("client:" + 0);
        ProcessId pid1 = new ProcessId("client:" + 1);

        cid = 0; r = 0;
        sendMessage(replicas[r], new RequestMessage(pid0, new Command(pid0, cid, "operation " + 1)));
        
        cid = 0; r = 1;
        sendMessage(replicas[r], new RequestMessage(pid1, new Command(pid1, cid, "operation " + 2)));
        
        cid = 0; r = 1;
        sendMessage(replicas[r], new RequestMessage(pid0, new Command(pid0, cid, "operation " + 3)));
        
        cid = 0; r = 0;
        sendMessage(replicas[r], new RequestMessage(pid1, new Command(pid1, cid, "operation " + 4)));
        */
        
        
    }

    public static void main(String[] args) {
        new Env().run(args);
    }
}
