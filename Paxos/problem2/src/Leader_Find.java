import java.util.*;

/**
 *
 * @author xwang
 */
public class Leader_Find {
    // ProcessId leader; // the current Leader's ProcessId
    Env env; 
    ProcessId[] acceptors;
    ProcessId[] leaders;
    Map<ProcessId, Integer> Distribution = new HashMap<ProcessId, Integer>();
    
    public Leader_Find(Env env) {
        this.env = env;
        this.acceptors = env.acceptors;
        this.leaders = env.leaders;
        reset();
    }
    
    public ProcessId getCurrentLeaderId() {
        reset();
        
        for (ProcessId accId : acceptors) {
            Acceptor acc = (Acceptor) env.procs.get(accId);
            ProcessId leaderId = acc.getCurrentLeaderId();
            int leaderIdNum = Distribution.get(leaderId);
            
            //System.out.println("oooo " + leaderId);
            
            leaderIdNum ++;
            Distribution.put(leaderId, leaderIdNum);
        }
        
        /*This is very very very IMPORTANT!!!!*/
        /* maxAcceptorNum is the number a majority of acceptors*/
        int maxAcceptorNum = (int)((env.nAcceptors)/2) + 1;  // set this manually .....
        /*This is very very very IMPORTANTT!!!*/
        
        
        ProcessId maxLeaderId = new ProcessId("Fuck");
        
        for (ProcessId leaderId : Distribution.keySet()) {
            if (Distribution.get(leaderId) >= maxAcceptorNum) {
                maxAcceptorNum = Distribution.get(leaderId);
                maxLeaderId = leaderId;
            } 
        }
        
        //System.out.println("I will print the Distributio Table!!!!");
        //for (ProcessId leaderId : Distribution.keySet()) {
            //System.out.println(leaderId + "    " + Integer.toString(Distribution.get(leaderId)));
        //}
        //System.out.println("I love qi" + maxLeaderId);
        return maxLeaderId;
        
    }
    
    public void reset() {
        for (ProcessId leaderTmp : leaders) {
            Distribution.put(leaderTmp, 0);
        }
    }
}
