
import java.util.*;

public class PaxosMessage {

    ProcessId src;
    //@Override
    //public abstract String toString();
}

class P1aMessage extends PaxosMessage {

    BallotNumber ballot_number;

    P1aMessage(ProcessId src, BallotNumber ballot_number) {
        this.src = src;
        this.ballot_number = ballot_number;
    }

    //@Override
    public String toString() {
        return "P1aMessage(ProcessID:" + src + "," + ballot_number + ")";
    }
}

class P1bMessage extends PaxosMessage {

    BallotNumber ballot_number;
    Set<PValue> accepted;

    P1bMessage(ProcessId src, BallotNumber ballot_number, Set<PValue> accepted) {
        this.src = src;
        this.ballot_number = ballot_number;
        this.accepted = accepted;
    }
    //Can I print all the information in accepted set?
    //@Override

    public String toString() {
        return "P1bMessage(Process ID:" + src + "," + ballot_number + ")";
    }
}

class P2aMessage extends PaxosMessage {

    BallotNumber ballot_number;
    int slot_number;
    Command command;

    P2aMessage(ProcessId src, BallotNumber ballot_number, int slot_number, Command command) {
        this.src = src;
        this.ballot_number = ballot_number;
        this.slot_number = slot_number;
        this.command = command;
    }

    //@Override
    public String toString() {
        return "P2aMessage(Process ID:" + src + "," + ballot_number + "slot_bumber" + slot_number + "command" + ")";
    }
}

class P2bMessage extends PaxosMessage {

    BallotNumber ballot_number;
    int slot_number;

    P2bMessage(ProcessId src, BallotNumber ballot_number, int slot_number) {
        this.src = src;
        this.ballot_number = ballot_number;
        this.slot_number = slot_number;
    }
    //@Override

    public String toString() {
        return "P2bMessage(ProcessID" + src + "," + ballot_number + "slot_number" + slot_number + ")";
    }
}

class PreemptedMessage extends PaxosMessage {

    BallotNumber ballot_number;

    PreemptedMessage(ProcessId src, BallotNumber ballot_number) {
        this.src = src;
        this.ballot_number = ballot_number;
    }

    public String toString() {
        return "PreemptedMessage(" + src + "," + ballot_number + ")";
    }
}

class AdoptedMessage extends PaxosMessage {

    BallotNumber ballot_number;
    Set<PValue> accepted;

    AdoptedMessage(ProcessId src, BallotNumber ballot_number, Set<PValue> accepted) {
        this.src = src;
        this.ballot_number = ballot_number;
        this.accepted = accepted;
    }

    public String toString() {
        return "AdoptedMessage: Proposing_Leader_ID: " + src + " ballot number: " + ballot_number;
    }
}

class DecisionMessage extends PaxosMessage {

    ProcessId src;
    int slot_number;
    Command command;

    public DecisionMessage(ProcessId src, int slot_number, Command command) {
        this.src = src;
        this.slot_number = slot_number;
        this.command = command;
    }
    //@Override

    public String toString() {
        return "DecisionMessage(" + src + ", slot_number" + slot_number + "," + command + ")";
    }
}

class RequestMessage extends PaxosMessage {

    Command command;

    public RequestMessage(ProcessId src, Command command) {
        this.src = src;
        this.command = command;
    }

    //@Override
    public String toString() {
        return "RequestMessage(Process ID:" + src + ", " + command + ")";
    }
}

class ProposeMessage extends PaxosMessage {

    int slot_number;
    Command command;

    public ProposeMessage(ProcessId src, int slot_number, Command command) {
        this.src = src;
        this.slot_number = slot_number;
        this.command = command;
    }

    public String toString() {
        return "ProposeMessage(Process ID:" + src + ", slot bumber:" + slot_number + ", " + command + ")";
    }
}

class ResponseMessage extends PaxosMessage {
    double result;//state of the account
    Command command;
    
    public ResponseMessage(ProcessId src, double result, Command command) {
        this.src = src;
        this.result = result;
        this.command = command;//find the original source according to <K, cid, operation>
    }
    public String toString() {
        return "ResponseMessage(ProcessID:" + src +", " + "result:" + result + "," + command +")";
    }
}

class ExecutionreqMessage extends PaxosMessage {
    
    Command command;
    
    public ExecutionreqMessage(ProcessId src, Command command) {
        this.src = src;
        this.command = command;
    }

    //@Override
    public String toString() {
        return "ExecutionreqMessage(Process ID:" + src + ", " + command + ")";
    }
    
}

// for failure detection
class PingMessage extends PaxosMessage {
    
    public PingMessage(ProcessId src) {
        this.src = src;
    }
    public ProcessId getSrc() {
        return this.src;
    }
    
    public String toString() {
        return "Ping Process ID: " + src;
    }
}

// for an actuall leader to respond
class PingResponseMessage extends PaxosMessage {
    public PingResponseMessage(ProcessId src) {
        this.src = src;
    }
    
    public ProcessId getSrc() {
        return this.src;
    }
    
    public String toString() {
        return "PingResponse Process ID: " + src;
    }
}

class BackFailureDetectorMessage extends PaxosMessage {
     BallotNumber ballot_number;
     
     public BackFailureDetectorMessage(ProcessId src, BallotNumber ballot_number) {
        this.src = src;
        this.ballot_number = ballot_number;
    }

    //@Override
    public String toString() {
        return "BackFailureDetectorMessage(ProcessID:" + src + "," + ballot_number + ")";
    }
    
     
}
