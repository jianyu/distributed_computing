/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jianyu
 */
public class Client_Thread extends Process{
    
    ProcessId[] replicas;
    int req_id;
    Queue<PaxosMessage> inbox2 = new Queue<PaxosMessage>();
    //Map<Integer, Boolean> recvReqid = new HashMap<Integer, Boolean>();
    
    public Client_Thread(Env env, ProcessId me, ProcessId[] replicas) {
        this.env = env;
        this.replicas = replicas;
        this.me = me;
        env.addProc(me, this);
    }
    
    public void body () {
        System.out.println("Here I am: " + me);

        {
            PaxosMessage msg2 = getNextMessage2();

            if (msg2 instanceof ExecutionreqMessage) {
                ExecutionreqMessage m2 = (ExecutionreqMessage) msg2;

                for (ProcessId r : replicas) {
                    sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op)));
                }
                req_id++;
            }
        }


        for (;;) {
            PaxosMessage msg = getNextMessage();

            if (msg instanceof ResponseMessage) {
                ResponseMessage m = (ResponseMessage) msg;
                env.logger.info(me + " receive " + m.toString());
                
                    //System.out.println(me + " receive " + m.toString());
                     //System.out.println(me + " result:" + m.result);

                    //m.command.req_id
                    //System.out.println(this.me + "req_id" + req_id + "message command req_id" + m.command.req_id);
                
                /*
                try {
                Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Client_Thread.class.getName()).log(Level.SEVERE, null, ex);
                }
                 * 
                 */
                
                if (this.req_id == (m.command.req_id + 1)) {
                    PaxosMessage msg2 = getNextMessage2();
                    if (msg2 instanceof ExecutionreqMessage) {
                        
                        
                        
                        ExecutionreqMessage m2 = (ExecutionreqMessage) msg2;
                                               
                        RequestMessage reqMessage = new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op));
                        env.logger.info(this.me + " send message to " + reqMessage + " to replica");//+ replicas[r]);
                        //System.out.println(this.me + " send message to " + reqMessage + " to replica");
                        
                        for (ProcessId r : replicas) {
                            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op)));
                        }
                        req_id++;
                    }
                    //System.out.println(this.me + "req_id:" + req_id);
                   
                }

            }

        }
    }
    
    PaxosMessage getNextMessage2(){
        return inbox2.bdequeue();
    }

    void deliver2(PaxosMessage msg) {
        inbox2.enqueue(msg);
    }

    
    
    
}
