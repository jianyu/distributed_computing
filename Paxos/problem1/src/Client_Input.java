/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class Client_Input {
    
    //sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m.command.op)));
    
    ProcessId[] replicas;
    int req_id;
    
    public Client_Input(Env env, ProcessId me, ProcessId[] replicas) {
        this.env = env;
        this.replicas = replicas;
        this.me = me;
        env.addProc(me, this);
    }
    
    public void body () {
        System.out.println("Here I am: " + me);

                    Operation op = new Create(0);
        sendMessage(0, new RequestMessage(this.me, new Command(this.me, this.req_id, op)));
            
            
            
            
            PaxosMessage msg = getNextMessage();
            
            if (msg instanceof ResponseMessage) {
                ResponseMessage m = (ResponseMessage) msg;
                System.out.println("result:"+m.result);
                
            }
            
            else if (msg instanceof ExecutionreqMessage) {
                ExecutionreqMessage m = (ExecutionreqMessage) msg;
                
                for (ProcessId r: replicas) {
                    sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m.command.op)));
                }
                req_id++;
                
            }

    }

    public static void main(String[] args) {
        //new Client_Input().run(args);
        
        
        Operation op = new Create(0);
        sendMessage(0, new RequestMessage(this.me, new Command(this.me, this.req_id, op)));
        
        
        
    }
    
}
