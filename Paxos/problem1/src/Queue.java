
import java.util.*;

public class Queue<T> {

    LinkedList<T> ll = new LinkedList<T>();

    public synchronized void enqueue(T obj) {
        ll.add(obj);
        notify();
    }

    public synchronized T bdequeue() {
        while (ll.size() == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        return ll.removeFirst();
    }

    /*
    //if we don't wait, it will definitely return 0!!!!!   But we have sleept before call this function...
    public synchronized int getqueueSize() {
        return ll.size();
    }*/

    
    public synchronized int getqueueSize(int timeout) {
        try { wait(timeout, 0); } catch (InterruptedException e) {}
        return ll.size();
    }
    
}
