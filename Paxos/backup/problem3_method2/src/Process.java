
public abstract class Process extends Thread {

    ProcessId me;
    Queue<PaxosMessage> inbox = new Queue<PaxosMessage>();
    Env env;

    abstract void body();

    public void run() {
        body();
        env.removeProc(me);
    }

    PaxosMessage getNextMessage() {
        return inbox.bdequeue();
    }
    
    PaxosMessage getNextMessage(int timeout) {
        return inbox.bdequeue(timeout);
    }

    int getMessageSize(int timeout) {
        return inbox.getqueueSize(timeout);
    }

    void sendMessage(ProcessId dst, PaxosMessage msg) {
        env.sendMessage(dst, msg);
    }

    void deliver(PaxosMessage msg) {
        inbox.enqueue(msg);
    }
}
