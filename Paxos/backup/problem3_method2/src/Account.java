/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class Account {
    double balance;
    
    public Account () {
        this.balance = 0;
    }
    
    public double deposit(double amount) {
        this.balance += amount;
        return this.balance;
    }

    public double withdraw(double amount) {
        if (this.balance < amount) {
            return -1;
        } else {
            this.balance -= amount;
            return this.balance;
        }
    }
    
    public double inquiry() {
        return this.balance;
    }
    
    
    public String toString () {
        return "Balance is " + this.balance;
    }

}
