/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jianyu
 */
public class Client_Thread extends Process{
    
    ProcessId[] replicas;
    int req_id;
    Queue<PaxosMessage> inbox2 = new Queue<PaxosMessage>();
    List<ProcessId> ResponsorList = new ArrayList<ProcessId>();

    //Map<Integer, Boolean> recvReqid = new HashMap<Integer, Boolean>();
    
    public Client_Thread(Env env, ProcessId me, ProcessId[] replicas) {
        this.env = env;
        this.replicas = replicas;
        this.me = me;
        env.addProc(me, this);
    }
    
    public void printResponsorList() {
        for (ProcessId responsor: ResponsorList) {
            System.out.println("#####"+responsor);
        }
    }
    
    public void body () {
        System.out.println("Here I am: " + me);
        PaxosMessage msg2;
        ExecutionreqMessage m2 = null;
        {
            msg2 = getNextMessage2();

            if (msg2 instanceof ExecutionreqMessage) {
                m2 = (ExecutionreqMessage) msg2;

                for (ProcessId r : replicas) {
                    sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op)));
                }
                req_id++;
            }
        }


        for (;;) {
            
            
            PaxosMessage msg;
            while (true) {
                
                //I still don't know how to use timeout for wait(), so I cannot guarantee the correctness of the following code
                msg = getNextMessage(5000);
                if (msg == null) {
                    if (!ResponsorList.isEmpty()) {
                        for (ProcessId responsor : ResponsorList) {
                            if (m2 == null) {
                                System.out.println("m2 is null!!!!");
                            } else {
                                sendMessage(responsor, new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op)));
                            }

                        }

                    } else {
                        System.out.println("All Replicas have crashed!!!");
                        //exit(0);
                        return;
                    }
                } else {
                    break;
                }

            }

            if (msg instanceof ResponseMessage) {
                
                ResponseMessage m = (ResponseMessage) msg;
                env.logger.info(me + " receive " + m.toString());
                
                
                //if (!ResponsorList.contains(m.src)) {
                //    ResponsorList.add(m.src);
                //}
                
                
                
                    //System.out.println(me + " receive " + m.toString());
                     //System.out.println(me + " result:" + m.result);

                    //m.command.req_id
                    //System.out.println(this.me + "req_id" + req_id + "message command req_id" + m.command.req_id);
                
                /*
                try {
                Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Client_Thread.class.getName()).log(Level.SEVERE, null, ex);
                }
                 * 
                 */
                
                if (this.req_id == (m.command.req_id + 1)) {
                    

                    
                    System.out.println(me + "command" + m.command + " result:" + m.result);
                    
                    
                    //if there is nothing in inbox2, then block here??????
                    msg2 = getNextMessage2();

                    if (msg2 instanceof ExecutionreqMessage) {
                        

                        m2 = (ExecutionreqMessage) msg2;

                        Operation opr = (Operation) m2.command.op;
                        if (opr instanceof Inquiry) {
                            
                            //Do nothing
                            
                        } else {
                            ResponsorList.clear();
                        }
                        
                        //add the source of m!!!!! It's m!!!!!!!!!!!!!!! the responseMessage
                        //rule out the deplicated...because for the consecutive read-only, we may get deplicated m.src from the same replica(because we didn't clear the responseList for the read-only operation
                        
                        if (!ResponsorList.contains(m.src)) {
                            ResponsorList.add(m.src);
                        }

                       
                        RequestMessage reqMessage = new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op));
                        env.logger.info(this.me + " send message to " + reqMessage.toString() + " to replica");//+ replicas[r]);
                        //System.out.println(this.me + " send message to " + reqMessage + " to replica");
                        
                        
                        printResponsorList();
                        for (ProcessId r : ResponsorList) {
                            sendMessage(r, new RequestMessage(this.me, new Command(this.me, this.req_id, m2.command.op)));
                        }
                        req_id++;
                    }
                    //System.out.println(this.me + "req_id:" + req_id);
    
                } else {
                    
                    //Actually, there is no duplicated cases....because here only the updating command response can come here???
                    //No....if one replica fails, the read-only command will also flooding to all in ResponsorList...
                    if (!ResponsorList.contains(m.src)) {
                        ResponsorList.add(m.src);
                    }

                }

            } 
            //no else....because there are only ResponseMessage in inbox2....

        }
    }
    
    PaxosMessage getNextMessage2(){
        return inbox2.bdequeue();
    }

    void deliver2(PaxosMessage msg) {
        inbox2.enqueue(msg);
    }

    
    
    
}
