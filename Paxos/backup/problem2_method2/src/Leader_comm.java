import java.util.*;

/**
 *
 * @author xwang
 */
public class Leader_comm extends Process {
    ProcessId currentLeaderId; 
    boolean flag; 
    // false: 
    ProcessId myLeaderId;
    
    public Leader_comm(Env env, ProcessId me, ProcessId currentLeaderId, ProcessId myLeaderId) {
        this.env = env;
        this.me = me;
        this.currentLeaderId = currentLeaderId;
        this.flag = true;
        this.myLeaderId = myLeaderId;
        
        env.addProc(me, this);
    }
    
    public void setCurrentLeaderId(ProcessId currentLeaderId) {
        this.currentLeaderId = currentLeaderId;
    }
    
    void body() {
        PingMessage pingMsg = new PingMessage(me);
        
        System.out.println(this.me + " is in the Comm function!!!!!!!!!!!");
        
        while (flag) {
            sendMessage(currentLeaderId, pingMsg);
            System.out.println(this.me + " is pinging " + currentLeaderId);
            flag = tryNextMessage(40);
            System.out.println(this.me + "The flag of this pinging is " + flag);
            //System.out.println(this.me + " comes in the LOOP!!!!!!");
        }
        
        System.out.println(this.me + " cannot ping " + currentLeaderId);
        Leader l = (Leader) env.procs.get(this.myLeaderId);
        l.setFlag(false);
    }
    
}
