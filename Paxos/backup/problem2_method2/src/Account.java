/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xwang
 */
public class Account {
    double balance;
    //boolean opFlag;
    // opFlag = false : there is no current operation, i.e., if receiving responseMessage, just igonore the msg.
    // opFlag = true: there is a current operation, i.e., if receiving responseMessage, operate and set opFlag to false.
    
    public Account() {
        this.balance = 0;
        //this.opFlag = false;
    }
    
    public void deposit(double deposit) {
        this.balance += deposit;
        
        return;
    }
    
    public boolean withdraw(double withdraw) {
        if (this.balance < withdraw) {
            return false;
        } else {
            this.balance -= withdraw;
            return true;
        }
    }
    
    /*
    public boolean transfer(double transfer, Account account) {
        if (this.withdraw(transfer)) {
            account.deposit(transfer);
            return true;
        } else {
            return false;
        }
    }
     * 
     */
    
    public double inquiry() {
        return this.balance;
    }
    
    /*
    public void TOpFlag() {
        this.opFlag = true;
        
        return;
    }
    
    public void FOpFlag() {
        this.opFlag = false;
        
        return;
    }
    
    public boolean getOpFlag() {
        return this.opFlag;
    }
    
     * 
     */
    public String toString () {
        return "Balance is " + this.balance;
    }
    
    /*
    public boolean executable() {
        return !this.opFlag;
    }
     * 
     */
    
    public double getBalance() {
        return this.balance;
    }
}
