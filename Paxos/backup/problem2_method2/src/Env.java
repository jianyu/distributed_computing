import java.util.*;

public class Env {
	Map<ProcessId, Process> procs = new HashMap<ProcessId, Process>();
	public final static int nAcceptors = 5, nReplicas = 2, nLeaders = 3, nRequests = 3;

	synchronized void sendMessage(ProcessId dst, PaxosMessage msg){
		Process p = procs.get(dst);
		if (p != null) {
			p.deliver(msg);
		}
                
                //System.out.println("In inner sendMessage Function: " + dst);
	}

	synchronized void addProc(ProcessId pid, Process proc){
		procs.put(pid, proc);
                
		proc.start(); 
                // proc is a thread
                // in Java, to start a thread, you can just call:
                // proc.start()
	}

	synchronized void removeProc(ProcessId pid){
		procs.remove(pid);
	}

	void run(String[] args){
		ProcessId[] acceptors = new ProcessId[nAcceptors];
		ProcessId[] replicas = new ProcessId[nReplicas];
		ProcessId[] leaders = new ProcessId[nLeaders];

		for (int i = 0; i < nAcceptors; i++) {
			acceptors[i] = new ProcessId("acceptor:" + i);
			Acceptor acc = new Acceptor(this, acceptors[i]);
		}
                System.out.println("Acceptors created complete.");
		for (int i = 0; i < nReplicas; i++) {
			replicas[i] = new ProcessId("replica:" + i);
			Replica repl = new Replica(this, replicas[i], leaders);
		}
                System.out.println("Acceptors created complete.");
		for (int i = 0; i < nLeaders; i++) {
			leaders[i] = new ProcessId("leader:" + i);
			Leader leader = new Leader(this, leaders[i], acceptors, replicas, leaders);
		}
                System.out.println("Leaders created complete.");
                
                /*
		for (int i = 1; i < nRequests; i++) {
			ProcessId pid = new ProcessId("client:" + i);
			for (int r = 0; r < nReplicas; r++) {
				sendMessage(replicas[r], new RequestMessage(pid, new Command(pid, 0, "operation " + 0)));
			}
                        for (int r = 0; r < nReplicas; r++) {
				sendMessage(replicas[r], new RequestMessage(pid, new Command(pid, 1, "operation " + 1)));
				//sendMessage(replicas[r], new RequestMessage(pid, new Command(pid, 2, "operation " + 2)));
                        }
		}
                 * 
                 */
                
                
                /*
                ProcessId pid0 = new ProcessId("client:" + 0);
                ProcessId pid1 = new ProcessId("client:" + 1);
                ProcessId pid2 = new ProcessId("client:" + 2);

                sendMessage(replicas[0], new RequestMessage(pid0, new Command(pid0, 0, "operation " + 0)));
                sendMessage(replicas[0], new RequestMessage(pid1, new Command(pid1, 0, "operation " + 0)));
                sendMessage(replicas[1], new RequestMessage(pid2, new Command(pid2, 1, "operation " + 1)));
                sendMessage(replicas[1], new RequestMessage(pid0, new Command(pid0, 1, "operation " + 1)));
                sendMessage(replicas[1], new RequestMessage(pid1, new Command(pid1, 1, "operation " + 1)));
                sendMessage(replicas[0], new RequestMessage(pid2, new Command(pid2, 0, "operation " + 0)));
                */
                
                
                /*
                ProcessId clientpid0 = new ProcessId("Client: " + 0);
                ProcessId clientpid1 = new ProcessId("Client: " + 1);
                ProcessId clientpid2 = new ProcessId("Client: " + 2);
                Client client0 = new Client(this, clientpid0, replicas);
                Client client1 = new Client(this, clientpid1, replicas);
                Client client2 = new Client(this, clientpid2, replicas);
                
                client0.createAccount(0);
                client1.createAccount(0);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ie) {
                    //Handle exception
                }
                
                client0.deposit(0,100);
                client1.deposit(0,99);
                 * 
                 */
                
                Client_Thread client0 = new Client_Thread(this, 0, replicas);
                Client_Thread client1 = new Client_Thread(this, 1, replicas);
                
                Monitor monitorThread = new Monitor(this, leaders, acceptors);
                monitorThread.start();
                
                client0.start();
                client1.start();
                
                
                
                

        }

	public static void main(String[] args){
		new Env().run(args);
	}
}
