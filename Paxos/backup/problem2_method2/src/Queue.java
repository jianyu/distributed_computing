import java.util.*;

 public class Queue<T> {
	LinkedList<T> ll = new LinkedList<T>();

	public synchronized void enqueue(T obj){
		ll.add(obj);
		notify();
	}

	public synchronized T bdequeue(){
		while (ll.size() == 0) {
			try { wait(); } catch (InterruptedException e) {}
		}
		return ll.removeFirst();
	}
        
        // if can get some message, return true
        // else, return false
        public synchronized boolean getMessage(int timeout) {
            
            try { wait(timeout, 0); } catch (InterruptedException e) {}

            if (ll.size() == 0) {
                return false;
            } else {
                ll.removeFirst();
                return true;
            }
        }
        
}
