import java.util.*;

public class Replica extends Process {
	ProcessId[] leaders;
	int slot_num = 1;
	Map<Integer /* slot number */, Command> proposals = new HashMap<Integer, Command>();
	Map<Integer /* slot number */, Command> decisions = new HashMap<Integer, Command>();
        
        Map<String, Client> clientList = new HashMap<String, Client>();
        
	public Replica(Env env, ProcessId me, ProcessId[] leaders){
		this.env = env;
		this.me = me;
		this.leaders = leaders;
		env.addProc(me, this);
	}

	void propose(Command c){
		if (!decisions.containsValue(c)) {
			for (int s = 1;; s++) {
				if (!proposals.containsKey(s) && !decisions.containsKey(s)) {
					proposals.put(s, c);
					for (ProcessId ldr: leaders) {
                                            ProposeMessage tmp;
                                            sendMessage(ldr, tmp = new ProposeMessage(me, s, c)); 
                                            //System.out.println(tmp + " Receiving Process ID: " + ldr.toString());
					}
					break;
				}
			}
		}
	}

	void perform(Command c){
		for (int s = 1; s < slot_num; s++) {
			if (c.equals(decisions.get(s))) {
				slot_num++;
				return;
			}
		}
		//System.out.println("" + me + ": perform " + c);
		slot_num++;
                
                // Replicas should also send ResponseMessages to notify the clients
                ResponseMessage m = new ResponseMessage(this.me, c);
                sendMessage(c.getClient(), m);
                //System.out.println(this.me + " sends a Response Message to " + c.getClient() + " with contents " + m);
                
                // Further polishment: log the info into a file
                String[] split = c.getOp().toString().split("@");
                if (split[0].equals("create")) {
                    if (!clientList.containsKey(split[1])) {
                        Client clientTmp = new Client(split[1]);
                        String accountNumTmp = split[2];
                        clientTmp.createAccount(accountNumTmp);
                        this.clientList.put(split[1], clientTmp);
                    } else {
                        Client clientTmp = clientList.get(split[1]);
                        String accountNumTmp = split[2];
                        clientTmp.createAccount(accountNumTmp);
                        this.clientList.put(split[1], clientTmp);
                    }
                } else if (split[0].equals("deposit")) {
                    Client clientTmp = clientList.get(split[1]);
                    String accountNumTmp = split[2];
                    //Account accountTmp = clientTmp.accountList.get(accountNumTmp);
                    double depositTmp = Double.parseDouble(split[3]);
                    //accountTmp.deposit(depositTmp);
                    clientTmp.deposit(accountNumTmp, depositTmp);
                    //clientTmp.accountList.put(accountNumTmp, accountTmp);
                    clientList.put(split[1], clientTmp);
                } else if (split[0].equals("withdraw")) {
                    Client clientTmp = clientList.get(split[1]);
                    String accountNumTmp = split[2];
                    //Account accountTmp = clientTmp.accountList.get(accountNumTmp);
                    double withdrawTmp = Double.parseDouble(split[3]);
                    //accountTmp.withdraw(withdrawTmp);
                    clientTmp.withdraw(accountNumTmp, withdrawTmp);
                    //clientTmp.accountList.put(accountNumTmp, accountTmp);
                    clientList.put(split[1], clientTmp);
                } else if (split[0].equals("transfer")) {
                    Client fromClientTmp = clientList.get(split[1]);
                    //System.out.println(split[1]);
                    //System.out.println(fromClientTmp);
                    String fromAccountNumTmp = split[2];
                    double transferTmp = Double.parseDouble(split[5]);
                    fromClientTmp.withdraw(fromAccountNumTmp, transferTmp);
                    //Account fromAccountTmp = fromClientTmp.accountList.get(fromAccountNumTmp);
                    //fromAccountTmp.withdraw(transferTmp);
                    //fromClientTmp.accountList.put(fromAccountNumTmp, fromAccountTmp);
                    clientList.put(split[1], fromClientTmp);
                    
                    Client toClientTmp = clientList.get(split[3]);
                    String toAccountNumTmp = split[4];
                    //Account toAccountTmp = toClientTmp.accountList.get(toAccountNumTmp);
                    toClientTmp.deposit(toAccountNumTmp, transferTmp);
                    //toAccountTmp.deposit(transferTmp);
                    //toClientTmp.accountList.put(toAccountNumTmp, toAccountTmp);
                    clientList.put(split[3], toClientTmp);
                    //System.out.println(split[4]);
                    
                } else if (split[0].equals("inquery")) {
                    
                } else if (split[0].equals("sudoPrint")) {
                    System.out.println();
                    System.out.println(this.me);
                    System.out.println(this);
                    System.out.println(this.DecisionToString());
                }
	}

        public String DecisionToString() {
            String output = "Decision set: " + "\n";
            
            for (int slot_Num: this.decisions.keySet()) {
                output += this.decisions.get(slot_Num).toString() + "\n";
            }
            
            return output;
            
        }
        
        public String toString() {
            String output = "";
            for (String client: this.clientList.keySet()) {
                Client clientToString = this.clientList.get(client);
                output += client + "\n" + clientToString;
            }
            
            return output;
        }
        
	public void body(){
		System.out.println("Here I am: " + me);
		for (;;) {
			PaxosMessage msg = getNextMessage();

			if (msg instanceof RequestMessage) {
				RequestMessage m = (RequestMessage) msg;
				propose(m.command);
                                //System.out.println(m + " Receiving Process ID: " + this.me.toString());
			}

			else if (msg instanceof DecisionMessage) {
				DecisionMessage m = (DecisionMessage) msg;
				decisions.put(m.slot_number, m.command);
				for (;;) {
					Command c = decisions.get(slot_num);
					if (c == null) {
						break;
					}
					Command c2 = proposals.get(slot_num);
					if (c2 != null && !c2.equals(c)) {
						propose(c2);
					}
					perform(c);
				}
			}
			else {
				System.err.println("Replica: unknown msg type");
			}
		}
	}
}
