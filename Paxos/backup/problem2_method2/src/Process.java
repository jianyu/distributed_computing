public abstract class Process extends Thread {
	ProcessId me;
	Queue<PaxosMessage> inbox = new Queue<PaxosMessage>();
	Env env;

	abstract void body();

	public void run(){
            //System.out.println(this.me + "begins to Run!");
            body();
            env.removeProc(me);
	}

	PaxosMessage getNextMessage(){
                //System.out.println("In getNextMessage Function: " + me + " try to get next message");
		
            return inbox.bdequeue();
	}
        
        boolean tryNextMessage(int timeout) {
            return inbox.getMessage(timeout);
        }

	void sendMessage(ProcessId dst, PaxosMessage msg){
		env.sendMessage(dst, msg);
                
                //System.out.println("In sendMessage Function: " + dst + " is the destination");
	}

	void deliver(PaxosMessage msg){
		inbox.enqueue(msg);
                
                //System.out.println("In deliver Function: " + me + "wants to deliver something");
	}
        
}
