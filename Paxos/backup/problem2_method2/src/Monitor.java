import java.util.*;

/**
 *
 * @author xwang
 */
public class Monitor extends Thread {
    Env env;
    ProcessId[] leaderIds;
    ProcessId[] acceptorIds;
    
    public Monitor(Env env, ProcessId[] leaders, ProcessId[] acceptors) {
        this.env = env;
        this.leaderIds = leaders;
        this.acceptorIds = acceptors;
    }
    
    public void run() {
        body();
    }
    
    public void body() {
        for (int i = 0; i< 3000; i++) {
            System.out.println(i + "-th active mode: ");
            for (ProcessId leaderIdTmp : leaderIds) {
                Leader leaderTmp = (Leader) this.env.procs.get(leaderIdTmp);
                System.out.println(leaderTmp);
            }
            
            for (ProcessId leaderIdTmp : leaderIds) {
                Leader leaderTmp = (Leader) this.env.procs.get(leaderIdTmp);
                System.out.println(leaderTmp.getFlag());
            }
            
            for (ProcessId acceptorIdTmp : acceptorIds) {
                Acceptor acceptorTmp = (Acceptor) this.env.procs.get(acceptorIdTmp);
                System.out.println(acceptorTmp);
            }
            /*
            try {
                Thread.sleep(1);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }*/
        }
    }
}
