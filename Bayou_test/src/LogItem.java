import java.util.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class LogItem implements Comparable {
    // <CSN, acceptstamp, serverid>

    // commited sequence number
    // starting from 0
    // 0,1,2,3,.....
    int CSN;
    
    // accept-stamp
    // <acceptstamp, serverid>
    int acceptStamp;
    ServerId serverId;
    
    // operation to do
    Operation Opr;
    
    public LogItem(int CSN, int acceptStamp, ServerId serverId, Operation Opr) {
        this.CSN = CSN;
        this.acceptStamp = acceptStamp;
        this.serverId = serverId;
        this.Opr = Opr;
    }

    public String toString() {
        return "LogItem: CSN-" + CSN +" AS-" + acceptStamp + " " + serverId + " OPR-" + Opr;
    }

    public int compareTo(Object other) {
        LogItem logitem = (LogItem) other;
        if (logitem.CSN != CSN) {
            return (int)(CSN - logitem.CSN);
        } else if (logitem.acceptStamp != acceptStamp) {
            return (int)(acceptStamp - logitem.acceptStamp);
        } /*else if (logitem.serverId != serverId) {
            return serverId.compareTo(logitem.serverId);
        }*/else {
            return 1;
        }
    }
    
    //We need to debug
    //Compare the two items, if their acceptStamp and the serverId are the same.
    public boolean EqualsTo(Object other) {
        LogItem logitem = (LogItem) other;
        if (logitem.acceptStamp == this.acceptStamp && logitem.serverId.toString().equals(this.serverId.toString())) {
            return true;
        } else {
            return false;
        }
        
    }
    

    
    
    
}
