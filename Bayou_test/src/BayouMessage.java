import java.util.*;


public class BayouMessage {
    ProcessId src;

    public ProcessId getSrc() {
        return this.src;
    }
}


/*
 * Once a server is created, it sends a ServerCreateMessage to 
 * the current primay server to notify its creation
 */
class ServerCreateMessage extends BayouMessage {
    ServerCreateMessage(ProcessId src) {
        this.src = src;
    }
    
    public String toString() {
        return "ServerCreateMessage(SRC:" + src  + ")";
    }
}

/*
 * Once the primary server receives a ServerCreateMessage 
 * it replies a ServerCreateReplyMessage
 * which contains the serverId the new server is allocated
 */
class ServerCreateReplyMessage extends BayouMessage {
    ServerId serverId;
    
    ServerCreateReplyMessage(ProcessId src, ServerId serverId) {
        this.src = src;
        this.serverId = serverId;
    }
}

class ServerConnectMessage extends BayouMessage {
    
    ServerConnectMessage(ProcessId src) {
        this.src = src;
    }
}

class ServerRetirementMessage extends BayouMessage {
    ServerRetirementMessage(ProcessId src) {
        this.src = src;
    }
}

// this message is sent from env to client
// @param toServer: client sends Opr to toServer
// @param Opr: client's operation (add/delete/edit/read)
class OprMessageToClient extends BayouMessage {
    ProcessId toServer;
    Operation Opr;
    
    OprMessageToClient(ProcessId src, ProcessId toServer, Operation Opr) {
        this.src = src;
        this.toServer = toServer;
        this.Opr = Opr;
    }   
}

// this message is sent from client to server
// @param Opr: client's operation (add/delete/edit/read)
class OprMessageToServer extends BayouMessage {
    Operation Opr;
    Map<ServerId, Integer> sessionmanager = new HashMap<ServerId, Integer>();
    
    OprMessageToServer(ProcessId src, Operation Opr, Map<ServerId, Integer> sessionmanager) {
        this.src = src;
        this.Opr = Opr;
        this.sessionmanager = sessionmanager;
    }
    public String toString() {
        return "OprMessageToServer(SRC:" + src  + "Opr:" + Opr + /*"sessionmanager" + sessionmanager +*/")";
    }
}

class OprReply extends BayouMessage {
    Operation Opr;
    boolean success;
    Playlist playlist;
    LogItem logitem;
    //Map<ServerId, Long> versionVector = new HashMap<ServerId, Long>();
    
    OprReply(Operation Opr, boolean success, Playlist playlist, LogItem logitem) {
        this.Opr = Opr;
        this.success = success;
        this.playlist = playlist;
        this.logitem = logitem;
    }
    
    public String toString() {
        return "OprReply(Opr:" + Opr + ",success:" + success + ",playlist:" + playlist + ")";
    }
    
}

class AntiEntropyInit extends BayouMessage {
    
    AntiEntropyInit(ProcessId src) {
        this.src = src;
    }
    
}

class AntiEntropyReply extends BayouMessage {
    // version vector
    Map<ServerId, Integer> versionVector = new HashMap<ServerId, Integer>();
    int CSN_highest;
    LogList loglist;
    
    
    AntiEntropyReply(ProcessId src, Map<ServerId, Integer> versionVector, int CSN_highest, LogList loglist) {
        this.src = src;
        this.versionVector = versionVector;
        this.CSN_highest = CSN_highest;
        this.loglist = loglist;
    }
    
    
}

class CommitNotification extends BayouMessage {
    LogItem logitem;
    
    CommitNotification(ProcessId src, LogItem logitem) {
        this.src = src;
        this.logitem = logitem;
    }
}

class AntiEntropyLog extends BayouMessage {
    LogItem logitem;
    
    AntiEntropyLog(ProcessId src, LogItem logitem) {
        this.src = src;
        this.logitem = logitem;
    }
    
}

class YouArePrimary extends BayouMessage {
    int currentCSN;
    YouArePrimary(ProcessId src, int currentCSN) {
        this.src = src;
        this.currentCSN = currentCSN;
    }
}

/*
class AntiEntropyTerminate extends BayouMessage {
    
    AntiEntropyTerminate(ProcessId src) {
        this.src = src;
    }
}

 * 
 */


/*
class AddSuccessReply extends BayouMessage {
    
}

class EditSuccessReply extends BayouMessage {
    
}

class DeleteSuccessReply extends BayouMessage {
    
}

class StaleDataReply extends BayouMessage {
    
}
 * */