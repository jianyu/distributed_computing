import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class LogList {
    List LogList = new ArrayList<LogItem>();
    
    public LogList() {
        
    }
    
    public void LogListInsert(LogItem item) {
        // add item to the end of LogList
        LogList.add(item);
    }

    public void LogListSort() {
        // sort LogList accoding to
        // 1. CSN ascending order
        // following not implemented
        // 2. acceptStamp (time-stamp)
        // 3. serverid (used to break the tie of acceptStamp)
        Collections.sort(LogList);
    }
    
    public boolean LogListContainsServerId(ServerId serverid) {
        for (Object item: this.LogList) {
            LogItem logitem = (LogItem) item;
            if(logitem.serverId.toString().equals(serverid)) {
                return true;
            }
        }
        return false; 
    }
    
    public void printLog() {
        for (Object i: this.LogList) {
            LogItem item = (LogItem) i;
            System.out.println(item);
        }
    }
    
    public boolean containsServerid(ServerId serverid) {
        for (Object item: this.LogList) {
            LogItem logitem = (LogItem)item;
            if (logitem.serverId.toString().equals(serverid) ) {
                return true;
            }
        }
        return false;

    }
    
    

}
