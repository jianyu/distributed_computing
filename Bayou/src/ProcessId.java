public class ProcessId implements Comparable {
	String name;

	public ProcessId(String name){ this.name = name; }

	public boolean equals(Object other){
		return name.equals(((ProcessId) other).name);
	}

	public int compareTo(Object other){
		return name.compareTo(((ProcessId) other).name);
	}
        
        // this cannot be modified
	public String toString(){ return name; }
}
