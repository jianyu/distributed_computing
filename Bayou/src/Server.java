import java.util.*;

public class Server extends Process {
    // myServerId is the serverId of this server, which is used in creation and retirement
    ServerId myServerId;
    // true: this server is the primary server
    // false: not the primary server
    boolean isPrimaryServer;
    boolean toRetire;
    
    int TScounter;
    int CSNcounter;
    
    // Servers this server connects to
    List<ProcessId> toServerList = new ArrayList<ProcessId>();
    // Playlist
    Playlist PlayList = new Playlist();
    // Log
    LogList logList = new LogList();
    
    // version vector
    //Map<ServerId, Integer> versionVector = new HashMap<ServerId, Integer>();
    Map<String, Integer> versionVector = new HashMap<String, Integer>();

    // highest CSN
    int CSN_highest;
    
    // create ancestor server
    public Server (Env env, ProcessId me, ServerId serverId) {
        this.env = env;
        this.me = me;
        this.myServerId = serverId;
        this.isPrimaryServer = true;
        this.CSN_highest = 0;
        this.toRetire = false;
        this.TScounter = 0;
        this.CSNcounter = 0;
        this.versionVector.put(this.myServerId.toString(), 0);
        this.env.ServerId_ProcessId.put(this.myServerId.toString(), this.me);
        
        this.env.addProc(me, this);
    }
    
    // create following servers
    public Server (Env env, ProcessId me, ProcessId primaryServer) {
        this.env = env;
        this.me = me;
        this.isPrimaryServer = false;
        this.CSN_highest = 0;
        this.toRetire = false;
        this.TScounter = 0;
        this.CSNcounter = 0;
        /*
         * send the primary server a create messsage
         * to get my own server id which is gotten later in body()
         */
        sendMessage(primaryServer, new ServerCreateMessage(me));
        
        this.env.addProc(me, this);
    }
    
    public int genTScounter() {
        return ++(this.TScounter);
    }
    
    public int genCSNcounter() {
        return ++(this.CSNcounter);
    }
    
    public int setCSNcounter(int CSN){
        this.CSNcounter = CSN;
        return ++(this.CSNcounter);
    }
    
    public void printToServerList() {
        System.out.print(this.myServerId + " | " + this.me+"\n");
        for (Object obj: this.toServerList) {
            ProcessId pid = (ProcessId) obj;
            System.out.println(pid +" ");
        }
    }

    public String toString() {
        return "Print Process: "+this.me.toString();
    }

    public void toServerListAdd(ProcessId server) {
        this.toServerList.add(server);
    }
    
    public void toServerListDelete(ProcessId server) {
        this.toServerList.remove(server);
    }
    
    public void toServerListClear() {
        this.toServerList.clear();
    }
    
    public void printLog() {
        System.out.println("Log of [" + this.me + "] [" + this.myServerId + "] :");
        this.logList.printLog();
    }
    
    public void printPlayList() {
        System.out.println("PlayList of [" + this.me + "] [" + this.myServerId + "] :");
        this.PlayList.printPlayList();
    }
    
    // two things:
    // 1. update playlist
    // 2. add/delete entries in version vector
    public void updatePlayList() {
        this.PlayList.deleteAll();
        for (Object logitem: this.logList.LogList) {
            LogItem logitem1 = (LogItem) logitem;
            
            Operation Opr = logitem1.Opr;
            
            if(Opr instanceof Add) {
                this.PlayList.add(Opr.song, Opr.url);
            } else if (Opr instanceof Edit) {
                //
                this.PlayList.edit(Opr.song, Opr.url);
            } else if (Opr instanceof Delete) {
                this.PlayList.delete(Opr.song, Opr.url);
            } else if (Opr instanceof Read) {
                //Do nothing to the DB;
                //However, we should return DB to the client
            } else if (Opr instanceof CreateWrite) {
                // create a new entry in version vector
                CreateWrite Opr1 = (CreateWrite) Opr;
                //this.versionVector.put(Opr1.serverId, Opr1.acceptstamp); wrong!
                //this.versionVector.put(Opr1.serverId, 0); this is more wrong! it will rewrite the vv
                if (!this.versionVector.containsKey(Opr1.serverId.toString())) {
                    this.versionVector.put(Opr1.serverId.toString(), 0);
                }
            } else if (Opr instanceof RetireWrite) {
                // delete an entry in version vector
                RetireWrite Opr1 = (RetireWrite) Opr;
                //System.out.println("11111111111" + env.ServerId_ProcessId.get(Opr1.serverIdToString));
                //if (this.VVContainString(Opr1.serverIdToString)) {
                if (this.versionVector.containsKey(Opr1.serverIdToString)) {
                    //System.out.println("FFFFFFFFFFFFFF");
                    this.versionVector.remove(Opr1.serverIdToString);
                }
                //System.out.println("00000000000" + env.ServerId_ProcessId.get(Opr1.serverIdToString));
                if (this.toServerList.contains(env.ServerId_ProcessId.get(Opr1.serverIdToString))) {
                    //System.out.println("CCCCCCCCCCCCCC");
                    toServerListDelete(env.ServerId_ProcessId.get(Opr1.serverIdToString));
                }
            }
        }
    }
    
    
    /*
    boolean VVContainString(String serverIdToString) {
        for (ServerId serverId: this.versionVector.keySet()) {
            if (serverId.toString().equals(serverIdToString)) {
                return true;
            }
        }
        return false;
    }
    */

    // this anti-entropy is specified to one pair
    public void Anti_Entropy(BayouMessage msg) {
        // anti-entropy algorithm
        AntiEntropyReply m = (AntiEntropyReply) msg;        
        // Do anti-entropy once
        for (Object item : this.logList.LogList) {
            LogItem item1 = (LogItem) item;
            // item1's commit is known to that
            if (item1.CSN <= m.CSN_highest) {
                continue;
            } 
            // item1 is committed by me but unknown to that
            else if (item1.CSN < env.INF) {
                if (m.versionVector.containsKey(item1.serverId.toString())) {
                    if (item1.acceptStamp <= m.versionVector.get(item1.serverId.toString())) {
                        sendMessage(m.src, new CommitNotification(this.me, item1));
                    } else {
                        sendMessage(m.src, new AntiEntropyLog(this.me, item1));
                    }
                } else {
                    // check whether m.loglist contains log about item1.serverId
                    ///////////if(m.loglist.LogListContainsServerId(item1.serverId)) {
                    //Contains "CreateWrite" and "Retire"
                    //So item1.serverId has retired...
                    //do nothing
                    ///////////} else {
                    sendMessage(m.src, new AntiEntropyLog(this.me, item1));
                    /////////////}
                    // we do this check on the receiver side
                }
            } 
            // item1 is tentative in my log
            else {
                if (m.versionVector.containsKey(item1.serverId.toString())) {
                    if (item1.acceptStamp > m.versionVector.get(item1.serverId.toString())) {
                        sendMessage(m.src, new AntiEntropyLog(this.me, item1));
                    }
                } else {
                    // check whether m.loglist contains log about item1.serverId
                    ///////////////if (m.loglist.LogListContainsServerId(item1.serverId)) {
                    //Contains "CreateWrite" and "Retire"
                    //So item1.serverId has retired...
                    //do nothing
                    ////////////////} else {
                    sendMessage(m.src, new AntiEntropyLog(this.me, item1));
                    /////////////////}
                    // do this check on the receiver side
                }
            }
        }
    }
    
    public boolean StaleDataChecker(Map<String, Integer> vv, Map<String, Integer> sm) {
        for (String serverid: sm.keySet()) {
            if (!vv.containsKey(serverid)) {
                //Create & Retire
              if (this.logList.containsServerid(serverid)) {
                  continue;
                  //Totally New.....Then this guy is stale.....
              } else {
                  //System.out.println(serverid);
                  //System.out.println("FFFFFFFFFFFFFFFFFFFFFF");
                  return false;
              }
            } else if (vv.get(serverid) < sm.get(serverid)) {
                //System.out.println("GGGGGGGGGGGGGGGGGGG");
                return false;
            } 
        }
        return true;
    }
    
    // ProcessOpr is just for processing operations in some server
    // it is done only when a server receives a operation from a client
    // when rolling back and re-log, we dont do this
    // this is only for add/delete/edit
    public void ProcessOpr(Operation Opr, OprMessageToServer m) {
        //long acceptStamp = System.currentTimeMillis();
        int acceptStamp = this.genTScounter();
        LogItem toInsert;
        if (this.isPrimaryServer) {
            int csn = this.genCSNcounter();
            toInsert = new LogItem(csn, acceptStamp, this.myServerId, Opr);
            this.CSN_highest = csn;
        } else {
            toInsert = new LogItem(env.INF, acceptStamp, this.myServerId, Opr);
        }
        logList.LogListInsert(toInsert);
        logList.LogListSort();
        updatePlayList();
        versionVector.put(this.myServerId.toString(), acceptStamp);
        // this is to send the operation reply to a client to update its session manager
        sendMessage(m.src, new OprReply(Opr, true, this.PlayList, toInsert));
        
        for (ProcessId pid : toServerList) {
            sendMessage(pid, new AntiEntropyInit(this.me));
        }
    }
    
    public void AntiEntropyUpdate() {
        // now we do this sort externally
        //this.logList.LogListSort();
        this.updatePlayList();
        for (ProcessId pid : toServerList) {
            sendMessage(pid, new AntiEntropyInit(this.me));
        }
    }
    
    public void commitAll() {
        for (Object item: this.logList.LogList) {
            LogItem logitem = (LogItem)item;
            if(logitem.CSN == env.INF) {
                logitem.CSN = this.genCSNcounter();
                this.CSN_highest = logitem.CSN;
            } 
        }
    }
    
    public void vvPrint() {
        System.out.println(this.me + "'s version vector: ");
        //System.out.println(this.versionVector.isEmpty());
        for (String sid: this.versionVector.keySet()) {
            System.out.println(env.ServerId_ProcessId.get(sid) + "   " + sid+ "  latest AS:  " + this.versionVector.get(sid));
        }
    }
    
    public void body(){
        for (;;) {
            BayouMessage msg = getNextMessage();
            
            // a new server is created
            if (msg instanceof ServerCreateMessage) {
                // received by the parent of newly created server
                if (this.toRetire) {
                    continue;
                }
                
                ServerCreateMessage m = (ServerCreateMessage) msg;
                int acceptStamp = this.genTScounter();
                // Allocate <accept-stamp, sid> as the new ServerID
                ServerId toAllocate = new ServerId(acceptStamp, this.myServerId);
                LogItem item;
                if (this.isPrimaryServer) {
                    int csn = this.genCSNcounter();
                    item = new LogItem(csn, acceptStamp, this.myServerId, new CreateWrite(toAllocate, acceptStamp));
                    this.CSN_highest = csn;
                } else {
                    item = new LogItem(env.INF, acceptStamp, this.myServerId, new CreateWrite(toAllocate, acceptStamp));
                }
                this.logList.LogListInsert(item);
                this.logList.LogListSort();
                // add an entry to the version vector
                this.versionVector.put(toAllocate.toString(), 0);                
                // update my entry in version vector
                this.versionVector.put(this.myServerId.toString(), acceptStamp);
                
                sendMessage(m.getSrc(), new ServerCreateReplyMessage(me, toAllocate, this.TScounter));
                /*
                 * CreateWrite should also do Anti-Entropy
                 */
                for (ProcessId pid : toServerList) {
                    sendMessage(pid, new AntiEntropyInit(this.me));
                }
            } 
            // this is received by the newly created server
            // from some server it firstly communicates to
            // this is used for assigning itself a serverId 
            else if (msg instanceof ServerCreateReplyMessage) {
                if (this.toRetire) {
                    continue;
                }

                ServerCreateReplyMessage m = (ServerCreateReplyMessage) msg;
                this.myServerId = m.serverId;
                
                this.TScounter = m.AStoInit; //AS is AcceptTimestamp, i.e., TimeStamp
                this.versionVector.put(this.myServerId.toString(), 0/*TScounter*/);
                
                env.ServerId_ProcessId.put(this.myServerId.toString(), this.me);
            } 
            // this is received by a server from a client 
            // who wants to send operations to this server
            else if (msg instanceof OprMessageToServer) {
                if (this.toRetire) {
                    continue;
                }

                OprMessageToServer m = (OprMessageToServer) msg;
                //System.out.println(this.me + " " + m);
                
                ProcessId src = m.src;
                Operation Opr = m.Opr;
                if (Opr instanceof Add) {
                    // insert this op to LogList
                    // sort LogList
                    // update db
                    // update version vector
                    // send reply to src
                    // do anti-entropy
                    ProcessOpr(Opr, m);
                } else if (Opr instanceof Edit) {
                    // this.versionVector V.S. Opr.sessionManager
                    // Edit Opr1 = (Edit) Opr;
                    if (StaleDataChecker(this.versionVector, m.sessionmanager)) {
                        ProcessOpr(Opr, m);
                    } else {
                        sendMessage(m.src, new OprReply(Opr, false, this.PlayList, null));
                    }                   
                } else if (Opr instanceof Delete) {
                    if (StaleDataChecker(this.versionVector, m.sessionmanager)) {
                        ProcessOpr(Opr, m);
                    } else {
                        sendMessage(m.src, new OprReply(Opr, false, this.PlayList, null));
                    }
                } else if (Opr instanceof Read) {
                    // insert this op to LogList
                    // sort LogList
                    // NO update db
                    // update version vector
                    // send reply to src
                    // do anti-entropy
                    Read Opr1 = (Read) Opr;
                    int acceptStamp = this.genTScounter();
                    LogItem toInsert;
                    if (this.isPrimaryServer) {
                        int csn = this.genCSNcounter();
                        this.CSN_highest = csn;
                        toInsert = new LogItem(csn, acceptStamp, this.myServerId, Opr1);
                    } else {
                        toInsert = new LogItem(env.INF, acceptStamp, this.myServerId, Opr1);
                    }
                    logList.LogListInsert(toInsert);
                    logList.LogListSort();
                    versionVector.put(this.myServerId.toString(), acceptStamp);
                    
                    sendMessage(m.src, new OprReply(Opr1, true, this.PlayList, toInsert));
                    for (ProcessId pid : toServerList) {
                        sendMessage(pid, new AntiEntropyInit(this.me));
                    }
                } 
            } else if (msg instanceof AntiEntropyReply) {
                //receive the AntiEntropyReply(which contains the version vector);
                Anti_Entropy(msg);
                
                //System.out.println(this.me + "AntiEntropyReply Received");
                if (this.toRetire) {
                    if (this.isPrimaryServer) {
                        // used for setting the new Primary's CSNcounter
                        sendMessage(msg.src, new YouArePrimary(this.me, this.CSNcounter));                     
                    }
                    break;
                }
            } else if (msg instanceof AntiEntropyInit) {
                if (this.toRetire) {
                    continue;
                }
                //System.out.println(this.me + "AntiEntropyInit Received");
                AntiEntropyInit m = (AntiEntropyInit) msg;
                sendMessage(m.src, new AntiEntropyReply(this.me, this.versionVector, this.CSN_highest));
            } else if (msg instanceof CommitNotification) {
                if (this.toRetire) {
                    continue;
                }
                CommitNotification m1 = (CommitNotification) msg;
                // only update the CSN and CSN_highest
                // update version vector, only for createwrite
                //boolean updateFlag = false;

                for (Object item : logList.LogList) {
                    LogItem item1 = (LogItem) item;
                    // .EqualsTo only checks ServerId & AcceptStamp
                    if (item1.EqualsTo(m1.logitem)) {
                        // check whether the server has committed, because multiple notifications
                        if (item1.CSN == env.INF) {
                            this.logList.LogList.remove(item1);
                            this.logList.LogList.add(m1.logitem);
                            this.CSN_highest = m1.logitem.CSN;
                            this.logList.LogListSort();
                            //updateFlag = true;
                            AntiEntropyUpdate();
                        }
                        break;
                    }
                }
            } else if (msg instanceof AntiEntropyLog) {
                if (this.toRetire) {
                    continue;
                }
                // as long as receiving this message, do followings
                // insert this to log
                // update version vector
                AntiEntropyLog m1 = (AntiEntropyLog) msg;
                
                if (!this.versionVector.containsKey(m1.logitem.serverId.toString())) {
                    if (!this.logList.containsServerid(m1.logitem.serverId.toString())) {
                        LogItem toInsert;
                        if (this.isPrimaryServer && m1.logitem.CSN == env.INF) {
                            int csn = this.genCSNcounter();
                            toInsert = new LogItem(csn, m1.logitem.acceptStamp, m1.logitem.serverId, m1.logitem.Opr);
                            this.CSN_highest = csn;
                        } else {
                            toInsert = new LogItem(m1.logitem.CSN, m1.logitem.acceptStamp, m1.logitem.serverId, m1.logitem.Opr);
                        }
                        this.logList.LogListInsert(toInsert);
                        this.logList.LogListSort();
                        this.versionVector.put(m1.logitem.serverId.toString(), m1.logitem.acceptStamp);
                        AntiEntropyUpdate();

                        /*
                        if (this.isPrimaryServer && m1.logitem.CSN == env.INF) {
                            m1.logitem.CSN = this.genCSNcounter();
                        } 
                        // add the entry
                        this.versionVector.put(m1.logitem.serverId.toString(), m1.logitem.acceptStamp);
                        this.logList.LogListInsert(m1.logitem);
                        this.logList.LogListSort();
                        if (m1.logitem.CSN < env.INF && m1.logitem.CSN > this.CSN_highest) {
                            this.CSN_highest = m1.logitem.CSN;
                        }
                        AntiEntropyUpdate(); 
                        */
                     } else {
                        // this is a retired log
                        // but it is potentially not committed, we need commit
                        if (m1.logitem.CSN < env.INF) {
                            for (Object item : this.logList.LogList) {
                                LogItem item1 = (LogItem) item;
                                // .EqualsTo only checks ServerId & AcceptStamp
                                if (item1.EqualsTo(m1.logitem)) {
                                    // check whether the server has committed, because multiple notifications
                                    if (item1.CSN == env.INF) {
                                        this.logList.LogList.remove(item1);
                                        this.logList.LogList.add(m1.logitem);
                                        if (this.CSN_highest < m1.logitem.CSN) {
                                            this.CSN_highest = m1.logitem.CSN;                                           
                                        }
                                        this.logList.LogListSort();
                                        AntiEntropyUpdate();
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } 
                // consider multiple logs
                else if (this.versionVector.get(m1.logitem.serverId.toString()) < m1.logitem.acceptStamp) {
                    // potential problems
                    LogItem toInsert;
                    if (this.isPrimaryServer) {
                        int csn = this.genCSNcounter();
                        toInsert = new LogItem(csn, m1.logitem.acceptStamp, m1.logitem.serverId, m1.logitem.Opr);
                        this.CSN_highest = csn;
                    } else {
                        toInsert = new LogItem(m1.logitem.CSN, m1.logitem.acceptStamp, m1.logitem.serverId, m1.logitem.Opr);
                    }
                    this.logList.LogListInsert(toInsert);
                    this.logList.LogListSort();
                    this.versionVector.put(m1.logitem.serverId.toString(), m1.logitem.acceptStamp);
                    AntiEntropyUpdate();
                    
                    /*
                    if (this.isPrimaryServer) {
                        m1.logitem.CSN = this.genCSNcounter();
                        this.CSN_highest = m1.logitem.CSN;
                    } 

                    this.logList.LogListInsert(m1.logitem);
                    this.logList.LogListSort();

                    //this.vvPrint();
                    // update the acceptstamp
                    this.versionVector.put(m1.logitem.serverId.toString(), m1.logitem.acceptStamp);

                    //System.out.println("Ilovewangqi  " + this.me);
                    //this.vvPrint();
                    // update CSN_highest
                    //AntiEntropyUpdate = true;
                    AntiEntropyUpdate();
                    */
                }
            } else if (msg instanceof ServerConnectMessage) {
                if (this.toRetire) {
                    continue;
                }
                
                // add ProcessId to toServerList
                ServerConnectMessage m = (ServerConnectMessage) msg;
                this.toServerListAdd(m.toConnect);
                
                
                //System.out.println(this.me + "ServerConnectionMessage Received");
                for (ProcessId pid : toServerList) {
                    sendMessage(pid, new AntiEntropyInit(this.me));
                }
            } else if (msg instanceof ServerRetirementMessage) {
                if (this.toRetire) {
                    continue;
                }
                
                //check isolation
                if (this.toServerList.isEmpty()) {
                    System.out.println(this.me + " is isolated, so it cannot retire");
                } else {
                    int acceptStamp = this.genTScounter();
                    RetireWrite Opr = new RetireWrite(this.myServerId.toString(), acceptStamp);
                    LogItem toInsert;
                    if (this.isPrimaryServer) {
                        // commit and allocate csn
                        int csn = this.genCSNcounter();
                        toInsert = new LogItem(csn, acceptStamp, this.myServerId, Opr);
                        this.CSN_highest = csn;
                    } else {
                        toInsert = new LogItem(env.INF, acceptStamp, this.myServerId, Opr);
                    }
                    logList.LogListInsert(toInsert);
                    logList.LogListSort();
                    versionVector.put(this.myServerId.toString(), acceptStamp);

                    for (ProcessId pid : toServerList) {
                        sendMessage(pid, new AntiEntropyInit(this.me));
                    }
                    this.toRetire = true;
                }
            } else if (msg instanceof YouArePrimary) {
                if (this.toRetire) {
                    continue;
                }
                this.isPrimaryServer = true;
                YouArePrimary m = (YouArePrimary) msg;
                this.CSNcounter = m.currentCSN;
                this.commitAll();
            }
        }
    }
}
