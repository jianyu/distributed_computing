
import java.util.*;

public class Client extends Process {
    /*
    // the server this client is currently connected to
    // this client always communicate with toServer
    ProcessId toServer;
     */

    // read your write
    // use something like version vector to do
    Map<String, Integer> sessionManager = new HashMap<String, Integer>();

    public Client(Env env, ProcessId me) {
        this.env = env;
        this.me = me;
        this.env.addProc(me, this);
    }
    

    /*
    public void toServerSet(ProcessId toServer) {
    this.toServer = toServer;
    }
     */
    public void body() {
        for (;;) {
            BayouMessage msg = getNextMessage();

            if (msg instanceof OprMessageToClient) {
                OprMessageToClient m = (OprMessageToClient) msg;
                ProcessId toServer = m.toServer;
                Operation Opr = m.Opr;
                if (Opr instanceof Add) {
                    // send an add message to toServer
                    Add Opr1 = (Add) Opr;
                    sendMessage(toServer, new OprMessageToServer(this.me, Opr1, sessionManager));
                } else if (Opr instanceof Edit) {
                    Edit Opr1 = (Edit) Opr;
                    sendMessage(toServer, new OprMessageToServer(this.me, Opr1, sessionManager));
                    
                } else if (Opr instanceof Delete) {
                    Delete Opr1 = (Delete) Opr;
                    sendMessage(toServer, new OprMessageToServer(this.me, Opr1, sessionManager));

                } else if (Opr instanceof Read) {
                    Read Opr1 = (Read) Opr;
                    sendMessage(toServer, new OprMessageToServer(this.me, Opr1, sessionManager));

                }
            } else if (msg instanceof OprReply) {

                //read -> we don't need to update the session manager
                OprReply m = (OprReply) msg;
                //System.out.println(this.me + "%%%%%%%%%%%%%%%" +m);
                if (m.Opr instanceof Read) {
                    if (m.success) {
                        System.out.println(this.me + " READ PlayList");
                        m.playlist.printPlayList();
                    } else {
                        System.out.println("Stale Data!");
                    }
                } else if (m.Opr instanceof Add) {
                    //update sessionmanager
                    // always seccessful
                    if (m.success) {
                        this.sessionManager.put(m.logitem.serverId.toString(), m.logitem.acceptStamp);
                    } else {
                        System.out.println("Error...cannot be here...Stale Data! Read Your Write guarantee that you connected to the stale server");
                    }

                } else {
                    if (m.success) {
                        this.sessionManager.put(m.logitem.serverId.toString(), m.logitem.acceptStamp);
                    } else {
                        System.out.println("Stale Data!");
                    }

                }
                
            }
        }
    }
}
