public class Operation {
    String song;
    String url;
    
}

class Add extends Operation {

    public Add (String song, String url) {
        this.song = song;
        this.url = url;
    }
    public Add (Add add1) {
        this.song = add1.song;
        this.url = add1.url;
    }
    public String toString() {
        return "[Add(song:" + song + ",url:" + url + ")]";
    }
    
}

class Edit extends Operation {

    public Edit (String song, String url) {
        this.song = song;
        this.url = url;
    }
    public String toString() {
        return "[Edit(song:" + song + ",url: " + url+ ")]";
    }
}

class Delete extends Operation {

    public Delete (String song) {
        this.song = song;
    }
    public String toString() {
        return "[Delete(song:" + song +")]";
    }
}

// read the current database
class Read extends Operation {
    
    public Read() {
        
    }
    
    public String toString() {
        return "[Read] ";
    }
    
}

// createwrite is for new server's creation
class CreateWrite extends Operation {
    ServerId serverId;
    int acceptstamp;
    
    public CreateWrite(ServerId serverId, int acceptstamp) {
        this.serverId = serverId;
        this.acceptstamp = acceptstamp;
    }
    
    public String toString() {
        return "[CreateWrite " + serverId + /*"," + acceptstamp+ */ "]";
    }
    
}

// retirewrite is for server's retirement
class RetireWrite extends Operation {
    //ServerId serverId;
    String serverIdToString;
    int acceptstamp;
    
    public RetireWrite(String serverIdToString, int acceptstamp) {
        this.serverIdToString = serverIdToString;
        this.acceptstamp = acceptstamp;
    }
    
    public String toString() {
        return "[RetireWrite " + serverIdToString + /*"," + acceptstamp +*/ "]";
    }
}
