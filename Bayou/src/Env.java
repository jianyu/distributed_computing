
import java.util.*;
import java.io.*;

public class Env {
    /*
     * This Map cannot be modified
     * it is used for message passing in the environment
     */

    Map<ProcessId, Process> procs = new HashMap<ProcessId, Process>();
    //Map<ServerId, ProcessId> ServerId_ProcessId = new HashMap<ServerId, ProcessId>();
    Map<String, ProcessId> ServerId_ProcessId = new HashMap<String, ProcessId>();
    ProcessId PrimaryServerProcessId;
    // infinite CSN
    //static final long INF = 9223372036854775807L;
    static final int INF = 65535;
    
    
    //ProcessId[] acceptors = new ProcessId[nAcceptors];
    

    synchronized void sendMessage(ProcessId dst, BayouMessage msg) {
        Process p = procs.get(dst);
        if (p != null) {
            p.deliver(msg);
        }
    }

    /*
    synchronized void sendMessage_AntiEntropyReply(ProcessId dst, BayouMessage msg) {
    Process p = procs.get(dst);
    if (p != null) {
    p.deliver_AntiEntropyReply(msg);
    }
    }
    
    synchronized void sendMessage_AntiEntropyLog(ProcessId dst, BayouMessage msg) {
    Process p = procs.get(dst);
    if (p != null) {
    p.deliver_AntiEntropyLog(msg);
    }
    }*/
    synchronized void addProc(ProcessId pid, Process proc) {
        //System.out.println("addProc: " + pid);
        //System.out.println("addProc: " + proc);

        procs.put(pid, proc);
        proc.start();
    }

    synchronized void removeProc(ProcessId pid) {
        procs.remove(pid);
    }
    
    public ProcessId genFirstServer() {
        for (ProcessId pid : this.procs.keySet()) {
            String[] str1 = pid.toString().split(":");
            if (str1[0].equals("Server")) {
                return pid;
            }
        }
        return null;
    }

    public ProcessId PID(String st) {
        for (ProcessId pid : procs.keySet()) {
            if(st.equals(pid.toString())) {
                return pid;
            }
        }
        System.out.println("No such st in procs!!!");
        return null;
    }
    
    synchronized public void printTopology() {
        System.out.println("**********************************************************************************");
        //System.out.println("Topology:");
        System.out.println("----------------------------------------------------------------------------------");
        for (ProcessId pid : procs.keySet()) {
            String[] str1 = pid.toString().split(":");
            if (str1[0].equals("Server")) {
                ((Server) procs.get(pid)).printToServerList();
                System.out.println("----------------------------------------------------------------------------------");
            }
        }
        System.out.println("**********************************************************************************");
    }

    void run(String[] args) {
        // initiate the first one server, which is the primary server, spwan a thread
        // whose address is "Process:0"

        PrimaryServerProcessId = new ProcessId("Server:0");
        Server primaryServer = new Server(this, PrimaryServerProcessId, new ServerId());

        ServerId_ProcessId.put((new ServerId()).toString(), PrimaryServerProcessId);

        while (true) {
            
            
            /*
            System.out.println("Topology:");
            for (ProcessId pid : procs.keySet()) {
                String[] str1 = pid.toString().split(":");
                if (str1[0].equals("Server")) {
                    ((Server)procs.get(pid)).printToServerList();
                }
            }*/
            
            //this.printTopology();
            
            String line = null;
            try {
                System.out.println("input:");
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                line = bufferRead.readLine();
                //System.out.println(line);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] str = line.split(" ");

            /*
             * Following are control commands
             * which can directly be operated using threads
             * and also can be conveyed by messages
             */
            if (str[0].equals("join")) {
                // format: join i
                // create a process with address "Server:i"
                // in that process's contruction function
                // communicate with the primary server and get the server id
 
                ProcessId newServerParent = genFirstServer();
                if (newServerParent != null) {
                    Server newServer = new Server(this, new ProcessId("Server:" + str[1]), newServerParent);
                } else { // never used
                    ProcessId pid = new ProcessId("Server:" + str[1]);
                    Server newServer = new Server(this, pid, new ServerId());
                    ServerId_ProcessId.put((new ServerId()).toString(), pid);
                }
                
            } else if (str[0].equals("leave")) {
                // format: leave i
                // notify the process with address "Server i"
                // to communicate with one server to retire, following the retirement protocol
                // (you need consider the retired one is the primary process) 
                ProcessId ProcessId_i = PID("Server:" + str[1]);
                sendMessage(ProcessId_i, new ServerRetirementMessage(new ProcessId("Controller")));
                
            } else if (str[0].equals("isolate")) {

                // format: isolate i
                // clear the toServerList of the process with address "Server i"
                // which allows that process to communicate with no process 
                String i = str[1];
                Server Server_i = (Server) procs.get(PID("Server:" + i));
                for (ProcessId s : Server_i.toServerList) {
                    Server s1 = (Server) procs.get(s);
                    s1.toServerListDelete(PID("Server:" + i));
                }
                Server_i.toServerListClear();

            } else if (str[0].equals("reconnect")) {
                // format: reconnet i
                // fulfill the toServerlist of the process with address "Server i"
                // with all the servers 
                String i = str[1];
                ProcessId ProcessId_i = PID("Server:" + i);
                Server Server_i = (Server) procs.get(ProcessId_i);
                //boolean flag = false;
                for (ProcessId s : this.procs.keySet()) {
                    String[] str1 = s.name.split(":");
                    if (str1[0].equals("Server") && !str1[1].equals(i)) {
                        if (!Server_i.toServerList.contains(s)) {
                            // we cannot add to toServerList and send msg here 
                            // because concurrency may lead to bad things
                            //Server_i.toServerListAdd(s); 
                            //Server other = (Server) procs.get(s);
                            //other.toServerListAdd(ProcessId_i); 
                            sendMessage(s, new ServerConnectMessage(new ProcessId("Controller"), ProcessId_i));
                            //flag = true;
                            /////////////////////////////////////////
                            sendMessage(ProcessId_i, new ServerConnectMessage(new ProcessId("Controller"), s));
                        }
                        //sendMessage(ProcessId_i, new ServerConnectMessage(new ProcessId("Controller"), ((Server)procs.get(s)).myServerId));
                    }
                }
                // sendMessage(ProcessId_i, new ServerConnectMessage(new ProcessId("Controller")));
                //if (flag) {
                    //sendMessage(ProcessId_i, new ServerConnectMessage(new ProcessId("Controller")));
                //}
                
            } else if (str[0].equals("breakconnection")) {
                // format: breakconnection i j
                // delete i from j's toServerList
                // delete j from i's toServerList
                String i = str[1];
                String j = str[2];

                Server Server_i = (Server) procs.get(PID("Server:" + i));
                Server_i.toServerListDelete(PID("Server:" + j));

                Server Server_j = (Server) procs.get(PID("Server:" + j));
                Server_j.toServerListDelete(PID("Server:" + i));
            } else if (str[0].equals("recoverconnection")) {
                // format: recoverconnection i j
                // add i in j's toServerList
                // add j in i's toServerList
                String i = str[1];
                String j = str[2];

                // we cannot add ProcessId to toServerList and send msg
                // because concurrency may lead to bat things
                ProcessId ProcessId_i = PID("Server:" + i);
                
                //((Server) procs.get(ProcessId_i)).toServerListAdd(PID("Server:" + j));

                ProcessId ProcessId_j = PID("Server:" + j);
                //Server Server_j = (Server) procs.get(ProcessId_j);
                //Server_j.toServerListAdd(PID("Server:" + i));

                sendMessage(ProcessId_i, new ServerConnectMessage(new ProcessId("Controller"), ProcessId_j));
                sendMessage(ProcessId_j, new ServerConnectMessage(new ProcessId("Controller"), ProcessId_i));

            } else if (str[0].equals("printlogall")) {
                // format: printlogall
                // print all servers' log
                // including committed and tentative
                System.out.println("**********************************************************************************");
                System.out.println("----------------------------------------------------------------------------------");
                for (ProcessId s : this.procs.keySet()) {
                    String[] str1 = s.name.split(":");
                    if (str1[0].equals("Server")) {
                        Server processToPrint = (Server) this.procs.get(s);
                        if (processToPrint.isPrimaryServer) {
                            System.out.println("I am Primary Server!");
                        }
                        processToPrint.printLog();
                        System.out.println("----------------------------------------------------------------------------------");

                    }
                }
                System.out.println("**********************************************************************************");
            } else if (str[0].equals("printlog")) {
                // format: printlog i
                // print the log of process with address "Server i"
                String i = str[1];
                Server Server_i = (Server) procs.get(PID("Server:" + i));
                System.out.println("**********************************************************************************");
                Server_i.printLog();
                System.out.println("**********************************************************************************");
            } else if (str[0].equals("printDBall")) {
                // format: printDBall
                // print all servers' database
                System.out.println("**********************************************************************************");
                System.out.println("----------------------------------------------------------------------------------");
                for (ProcessId s : this.procs.keySet()) {
                    String[] str1 = s.name.split(":");
                    if (str1[0].equals("Server")) {
                        Server processToPrint = (Server) this.procs.get(s);
                        processToPrint.printPlayList();
                        System.out.println("----------------------------------------------------------------------------------");
                    }
                }
                System.out.println("**********************************************************************************");
            } else if (str[0].equals("printDB")) {
                // format: printDB i
                // print database of process with address "Server i"
                String i = str[1];
                Server Server_i = (Server) procs.get(PID("Server:" + i));
                System.out.println("**********************************************************************************");
                Server_i.printPlayList();
                System.out.println("**********************************************************************************");
            } else if (str[0].equals("addclient")) {
                // format: addcilent i
                // create a new client with id "Client:i"
                Client newClient = new Client(this, new ProcessId("Client:" + str[1]));
                
            } else if (str[0].equals("vvprintall")) {
                // format: vvprintall
                // print all servers' version vector
                System.out.println("**********************************************************************************");
                System.out.println("----------------------------------------------------------------------------------");
                for (ProcessId s : this.procs.keySet()) {
                    String[] str1 = s.name.split(":");
                    if (str1[0].equals("Server")) {
                        Server processToPrint = (Server) this.procs.get(s);
                        //System.out.print(s + "***");
                        processToPrint.vvPrint();
                        System.out.println("----------------------------------------------------------------------------------");
                    }
                }
                System.out.println("**********************************************************************************");
            } else if (str[0].equals("printtopology")) {
                this.printTopology();
            }

            /* 
             * Following are operation commands
             * which are conveyed by messages
             */
            else if (str[0].equals("add")) {
                // format: add song url i j
                // send add message to client with address "Client i"
                // and that client will send add message to server with address "Server j"
                String song = str[1];
                String url = str[2];
                String i = str[3];
                String j = str[4];
                ProcessId c = PID("Client:" + i);
                ProcessId s = PID("Server:" + j);
                // here controller is just nothing
                // no Controller ProcessId in the hashmap
                sendMessage(c, new OprMessageToClient(new ProcessId("Controller"), s, new Add(song, url)));

            } else if (str[0].equals("edit")) {
                // format: edit song url_new i j
                // send edit message to client with address "Client i"
                // and that client will send edit message to server with address "Server j"
                String song = str[1];
                String url = str[2];
                String i = str[3];
                String j = str[4];
                ProcessId c = PID("Client:" + i);
                ProcessId s = PID("Server:" + j);

                sendMessage(c, new OprMessageToClient(new ProcessId("Controller"), s, new Edit(song, url)));
            } else if (str[0].equals("delete")) {
                // format: delete song i j
                // send delete message to client with address "Client i"
                // and that client will send delete message to server with address "Server j"
                String song = str[1];
                String i = str[2];
                String j = str[3];
                ProcessId c = PID("Client:" + i);
                ProcessId s = PID("Server:" + j);

                sendMessage(c, new OprMessageToClient(new ProcessId("Controller"), s, new Delete(song)));
            } else if (str[0].equals("read")) {
                // format: read i j
                //send read message to client with address "Client i"
                // and that client will send delete message to server with address "Server j"
                String i = str[1];
                String j = str[2];
                ProcessId c = PID("Client:" + i);
                ProcessId s = PID("Server:" + j);
                sendMessage(c, new OprMessageToClient(new ProcessId("Controller"), s, new Read()));
            } 
        }
    }

    public static void main(String[] args) {
        new Env().run(args);
    }
}
