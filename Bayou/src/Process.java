public abstract class Process extends Thread {
    ProcessId me;
    Queue<BayouMessage> inbox = new Queue<BayouMessage>();
    //Queue<BayouMessage> inbox_AntiEntropyReply = new Queue<BayouMessage>();
    //Queue<BayouMessage> inbox_AntiEntropyLog = new Queue<BayouMessage>();
    Env env;
    
    abstract void body();

    public void run() {
        body();
        env.removeProc(me);
    }

    BayouMessage getNextMessage() {
        return inbox.bdequeue();
    }
    
    //BayouMessage getNextMessage_AntiEntropyReply() {
    //    return inbox_AntiEntropyReply.bdequeue();
    //}
    
    //BayouMessage getNextMessage_AntiEntropyLog() {
    //    return inbox_AntiEntropyLog.bdequeue();
    //}

    void sendMessage(ProcessId dst, BayouMessage msg) {
        env.sendMessage(dst, msg);
    }
    
    //void sendMessage_AntiEntropyReply(ProcessId dst, BayouMessage msg) {
    //    env.sendMessage_AntiEntropyReply(dst, msg);
    //}
        
    //void sendMessage_AntiEntropyLog(ProcessId dst, BayouMessage msg) {
    //    env.sendMessage_AntiEntropyLog(dst, msg);
    //}

    void deliver(BayouMessage msg) {
        inbox.enqueue(msg);
    }
    
    //void deliver_AntiEntropyReply(BayouMessage msg) {
    //    inbox_AntiEntropyReply.enqueue(msg);
    //}
    
    //void deliver_AntiEntropyLog(BayouMessage msg) {
    //    inbox_AntiEntropyLog.enqueue(msg);
    //}
    
}
