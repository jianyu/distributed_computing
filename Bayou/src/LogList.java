import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jianyu
 */
public class LogList {
    List LogList = new ArrayList<LogItem>();
    
    public LogList() {
        
    }
    
    /*
    public LogList(LogList loglist1) {
        this.LogList = new ArrayList<LogItem>();      
        for(Object obj: loglist1.LogList) {
            LogItem logitem = new LogItem((LogItem)obj);
            this.LogList.add(logitem);
        }
    }
     * 
     */
    
    public void LogListInsert(LogItem item) {
        // add item to the end of LogList
        LogList.add(item);
    }

    public void LogListSort() {
        // sort LogList accoding to
        // 1. CSN ascending order
        // following not implemented
        // 2. acceptStamp (time-stamp)
        // 3. serverid (used to break the tie of acceptStamp)
        Collections.sort(LogList);
    }
    
    /*
    public boolean LogListContainsServerId(ServerId serverid) {
        
        for (Object item: this.LogList) {
            LogItem logitem = (LogItem) item;
            if(logitem.serverId.toString().equals(serverid.toString())) {
                return true;
            }
        }
        return false; 
    }*/
    
    public void printLog() {
        for (Object i: this.LogList) {
            LogItem item = (LogItem) i;
            System.out.println(item);
        }
    }
    
    /*
    public boolean containsServerid(ServerId serverid) {
        for (Object item: this.LogList) {
            LogItem logitem = (LogItem)item;
            if (logitem.serverId.toString().equals(serverid.toString()) ) {
                return true;
            }
        }
        return false;
    }*/
    
    
    public boolean containsServerid(String serveridToString) {
        for (Object item: this.LogList) {
            LogItem logitem = (LogItem)item;
            if (logitem.serverId.toString().equals(serveridToString) ) {
                return true;
            }
        }
        return false;
    }
    

}
