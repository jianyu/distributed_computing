/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xwang
 */
public class ServerId {
    
    int createTimeStamp; 
    
    ServerId serverId;
    // ancestorFlag: true -- the first one; false -- later servers
    // only the first one is true
    // even if the first one is retired, later servers are still false
    boolean ancestorFlag; 
    
    /*
     * This construction is used for constructing the first server
     * It is only used once
     */
    public ServerId() {
        this.createTimeStamp = 0;
        this.serverId = null;
        this.ancestorFlag = true;
    }
    
    public ServerId(ServerId serverid) {
        if(serverid.ancestorFlag) {
            this.createTimeStamp = serverid.createTimeStamp;
            this.serverId = null;
            this.ancestorFlag = serverid.ancestorFlag;
        } else {
            this.createTimeStamp = serverid.createTimeStamp;
            this.serverId = new ServerId(serverid.serverId);
            this.ancestorFlag = serverid.ancestorFlag;
        }
    }
    
    
    /* 
     * This construction function is used for constructing 
     * the serverIds of descendants which are created later
     * @param createTimeStamp: accept-stamp (causal-accept-stamp)
     * @param serverId: the serverId of 
     * @param ancestorFlag = false
     */
    public ServerId(int createTimeStamp, ServerId serverId) {
        this.createTimeStamp = createTimeStamp;
        this.serverId = serverId;
        this.ancestorFlag = false;
    }

    public String toString() {
        //System.out.println("");
        if (serverId == null){
            //System.out.println("--------------------------");
            return "SID-<ancestor>";
        } else {
            return "SID-<" + createTimeStamp +"," + serverId +">";
        }
        
    }

    /*
    public int compareTo(Object other) {
        ServerId serverid = (ServerId) other;
        
        return leader_id.compareTo(bn.leader_id);
    }
     * 
     */
}
