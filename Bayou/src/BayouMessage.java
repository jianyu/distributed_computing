import java.util.*;


public class BayouMessage {
    ProcessId src;

    public ProcessId getSrc() {
        return this.src;
    }
}


/*
 * Once a server is created, it sends a ServerCreateMessage to 
 * the current primay server to notify its creation
 */
class ServerCreateMessage extends BayouMessage {
    ServerCreateMessage(ProcessId src) {
        this.src = src;
    }
    
    public String toString() {
        return "ServerCreateMessage(SRC:" + src  + ")";
    }
}

/*
 * Once the primary server receives a ServerCreateMessage 
 * it replies a ServerCreateReplyMessage
 * which contains the serverId the new server is allocated
 */
class ServerCreateReplyMessage extends BayouMessage {
    ServerId serverId;
    int AStoInit;
    
    ServerCreateReplyMessage(ProcessId src, ServerId serverId, int AStoInit) {
        this.src = src;
        this.serverId = serverId;
        this.AStoInit = AStoInit;
    }
}

class ServerConnectMessage extends BayouMessage {
    ProcessId toConnect;
    
    ServerConnectMessage(ProcessId src, ProcessId toConnect) {
        this.src = src;
        this.toConnect = toConnect;
    }
}

class ServerRetirementMessage extends BayouMessage {
    ServerRetirementMessage(ProcessId src) {
        this.src = src;
    }
}

// this message is sent from env to client
// @param toServer: client sends Opr to toServer
// @param Opr: client's operation (add/delete/edit/read)
class OprMessageToClient extends BayouMessage {
    ProcessId toServer;
    Operation Opr;
    
    OprMessageToClient(ProcessId src, ProcessId toServer, Operation Opr) {
        this.src = src;
        this.toServer = toServer;
        this.Opr = Opr;
    }   
}

// this message is sent from client to server
// @param Opr: client's operation (add/delete/edit/read)
class OprMessageToServer extends BayouMessage {
    Operation Opr;
    Map<String, Integer> sessionmanager = new HashMap<String, Integer>();
    
    OprMessageToServer(ProcessId src, Operation Opr, Map<String, Integer> sessionmanager) {
        this.src = src;
        this.Opr = Opr;
        this.sessionmanager = sessionmanager;
    }
    public String toString() {
        return "OprMessageToServer(SRC:" + src  + "Opr:" + Opr + /*"sessionmanager" + sessionmanager +*/")";
    }
}

class OprReply extends BayouMessage {
    Operation Opr;
    boolean success;
    Playlist playlist;
    LogItem logitem;
    //Map<ServerId, Long> versionVector = new HashMap<ServerId, Long>();
    
    OprReply(Operation Opr, boolean success, Playlist playlist, LogItem logitem) {
        this.Opr = Opr;
        this.success = success;
        this.playlist = playlist;
        this.logitem = logitem;
        ///////////////////this.logitem = new LogItem(logitem);
    }
    
    public String toString() {
        return "OprReply(Opr:" + Opr + ",success:" + success + ",playlist:" + playlist + ")";
    }
    
}

class AntiEntropyInit extends BayouMessage {
    
    AntiEntropyInit(ProcessId src) {
        this.src = src;
    }
    
}

class AntiEntropyReply extends BayouMessage {
    // version vector
    Map<String, Integer> versionVector = new HashMap<String, Integer>();
    int CSN_highest;
    //LogList loglist;
    
    
    AntiEntropyReply(ProcessId src, Map<String, Integer> versionVector, int CSN_highest){////////////////////, LogList loglist) {
        this.src = src;
        this.versionVector = versionVector;
        this.CSN_highest = CSN_highest;
        //this.loglist = loglist;
        /////////////////////////////////////////this.loglist = new LogList(loglist);
    }
    
    
}

class CommitNotification extends BayouMessage {
    LogItem logitem;
    
    CommitNotification(ProcessId src, LogItem logitem) {
        this.src = src;
        this.logitem = logitem;
    }
}

class AntiEntropyLog extends BayouMessage {
    LogItem logitem;
    
    AntiEntropyLog(ProcessId src, LogItem logitem) {
        this.src = src;
        this.logitem = logitem;
    }
    
}

class YouArePrimary extends BayouMessage {
    int currentCSN;
    YouArePrimary(ProcessId src, int currentCSN) {
        this.src = src;
        this.currentCSN = currentCSN;
    }
}

/*
class AntiEntropyTerminate extends BayouMessage {
    
    AntiEntropyTerminate(ProcessId src) {
        this.src = src;
    }
}

 * 
 */


/*
class AddSuccessReply extends BayouMessage {
    
}

class EditSuccessReply extends BayouMessage {
    
}

class DeleteSuccessReply extends BayouMessage {
    
}

class StaleDataReply extends BayouMessage {
    
}
 * */